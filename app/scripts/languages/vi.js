/**
 * Created by dinhln on 25/05/2015.
 */

'use strict';

angular.module('TMSApp')
  .config(function ($translateProvider) {

  var translator = {
    common: {
      nadia: 'Nadia',
      tms: 'TMS',
      nav: 'Toggle navigation',
      search: 'Tìm kiếm',
      miss_input: 'Bạn nhập thiếu dữ liệu',
      log: {
        success: 'success',
        fail: 'fail',
        submit_success: 'submit success !',
        submit_fail: 'submit fail !',
        submit_begin: 'submit form'
      }
    },

    login: {
      signin_title: 'Đăng nhập hệ thống',
      signin: 'Đăng nhập',
      email: 'Email',
      password: 'Mật khẩu',
      forgot_password: 'Quên mật khẩu',
      register: 'Đăng ký tài khoản ',
      remember_me: 'Đăng nhập lần sau',
      email_required: 'Hãy nhập email',
      password_required:'Hãy nhập mật khẩu',
      email_error:'Không đúng định dạng Email. Hãy nhập lại',
      password_min:'Mật khẩu có tối thiểu 6 ký tự',
      messages:{
        success_login: 'Bạn đã đăng nhập thành công',
        error_login: 'Bạn đã đăng nhập không thành công'
      }
    },

    dashboard: {
      title: 'Bảng điều khiển',
      desc: 'Mô tả',
      hello: 'Xin chào',
      project: 'Project Dashboard',
      my_dashboard:'My Dashboard',
      ticket_chart: 'Ticket Chart',
      project_cost: 'Project- Hours',
      category_ticket: 'Category - Ticket',
      process_ticket: 'Process - Ticket',
      member_cost: 'Member - Hours',
      company_dashboard: 'Company Dashboard',
      ListProject:'List project'
    },

    menus: {
      project: 'Quản lý dự án',
      dashboard: 'Bảng điều khiển',
      reports: 'Báo cáo hàng ngày',
      missing_reports: 'Báo cáo nghỉ/quên report',
      users: 'Quản lý users',
      overtime: ' Quản lý Overtime ',
      overtime_report: ' Báo cáo Overtime',
      vacation: 'Quản lý Nghỉ',
      vacation_report: 'Báo cáo Nghỉ'
    },
    change_password: {
      change_title: 'Thay đổi mật khẩu',
      new_password: 'Mật khẩu mới',
      change_button: 'Thay đổi',
      cancel: 'Huỷ',
      messages:{
        success_change: 'Bạn đã thay đổi mật khẩu thành công',
        error_change: 'Bạn đã thay đổi mật khẩu không thành công'
      }
    },

    register: {
      login: 'Tôi đã có tài khoản.',
      register_title: 'Đăng ký người dùng mới',
      register_submit: 'Đăng ký',
      answer_required: 'Hãy nhập answer',
      name_required: 'Hãy nhập name',
      new_password: 'Mật khẩu mới của bạn: ',
      register_success: 'Bạn đã đăng ký thành công',
      register_error: 'Lỗi khi đăng ký'
    },

    forgot: {
      forgot_submit: 'Gửi',
      forgot_title: 'Lấy lại mật khẩu'
    },

    services: {
      user: {
        getQuestions: {
          fail: 'get questions fail',
          success: 'get questions success'
        }
      }
    },

    header: {
      logout: 'Đăng xuất'
    },

    report: {
      title: 'Báo cáo',
      search: 'Tìm kiếm',
      input: 'Báo cáo hàng ngày',
      missing: 'Báo cáo nghỉ/quên',
      daily: 'Báo cáo hàng ngày',
      member: 'Nhân Viên',
      dayoff: 'Nghỉ (ngày)',
      forgot: 'Quên (ngày)',
      day_off: 'Nghỉ',
      for_got:'Quên',
      vacation: 'Xin nghỉ',
      choose: 'Chọn thời gian tìm kiếm:',
      current: 'Tháng hiện tại:',
      date: 'Ngày',
      from: 'Bắt đầu',
      to : 'Kết thúc',
      hours: 'Số giờ',
      project: 'Dự án',
      ticket: 'Số thẻ ',
      category: 'Thể loại',
      process: 'Quá trình',
      note: 'Ghi chú',
      operation: 'Thao tác',
      total: 'Tổng thời gian:',
      day: 'Ngày',
      reason: 'Lý do',
      clear: 'Xoá',
      at: 'vào tháng',
      type: 'Trạng thái',
      accept:'Chấp nhận', 
      reject: 'Huỷ',
      accept_total: 'Tổng(Accepted): ',
      work_off: 'Work Off',
      weekend_time: 'Thời gian cuối tuần: ', 
      daily_time: 'Thời gian trong tuần: ',
      show: ' Số lượng báo cáo / trang : ',
      messages:{
        error_delete: 'Xoá không thành công',
        error_search: 'Tìm kiếm không thành công'
      }

    },
    project:{
      detail: 'Chi tiết',
      search: 'Tìm kiếm',
      index: 'STT',
      id: 'ID',
      deleteAction: 'Delete',
      name: 'Tên Dự Án',
      clear: 'Xoá',
      desc:'Danh sách',
      search_list:'Tìm kiếm các dự án',
      agree: 'Đồng ý',
      add_new: 'Add New Project',
      users: 'Số lượng NV',
      add_comfirm:'Thêm vào',
      close:'Đóng',
      add_user:'Thêm thành viên vào dự án',
      greater: 'Plan Time phải nhỏ hơn hoặc bằng Quotation Time',
      disable: 'Các dự án tạm dừng',
      enable: 'Các dự án hiện tại',
      deadline: 'Hạn chót nộp dự án',
       estimate: 'Estimate Time',
      messages:{
        error: 'Yêu cầu không thành công',
        error_search: 'Tìm kiếm không thành công'
      },
      add: {
        success: 'Thêm project thành công ! Hãy chọn danh sách user tham gia vào project !',
        please_add: 'Bạn hãy nhập tên project',
        add_user: 'Thêm Nhân viên vào Dự án ',
        please_choose:'Bạn hãy chọn Nhân viên !'
      },
      update: {
        success: 'Cập nhật thành công ',
      },
      delete: {
        confirm: 'Bạn muốn xoá người dùng với id: '
      },
      remove: {
        success: 'Xoá thành công'
      },
      edit: {
        title: 'Chỉnh sửa Dự án',
        not_data: 'Không có data cho project này'
      }
    },
    users: {
      list: 'Danh sách nhân viên',
      lock: 'Lock',
      unlock: 'Unlock',
      name: 'Tên nhân viên',
      edit: 'Chỉnh sửa ',
      match: 'Mật khẩu không trùng khớp',
      form_add: ' Thêm mới Nhân viên',
      profile: 'Profile Nhân viên ',
      text: 'Tìm kiếm'

    },
    setting: {
      category_management: 'Category Management', 
      process_management: 'Process Management', 
      create_setting: ' Create Setting',
       type: 'Type',

    },
  };

  $translateProvider.translations('vi', translator);

});
