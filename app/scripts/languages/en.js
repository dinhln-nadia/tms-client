/**
 * Created by nguyettt on 10/06/2015.
 */

'use strict';

angular.module('TMSApp')
  .config(function ($translateProvider) {

  var translator = {
    common: {
      nadia: 'Nadia',
      tms: 'TMS',
      nav: 'Toggle navigation',
      search: 'Search',
      miss_input: 'Missing data',
      log: {
        success: 'success',
        fail: 'fail',
        submit_success: 'submit success !',
        submit_fail: 'submit fail !',
        submit_begin: 'submit form'
      }
    },

    login: {
      signin_title: 'Login TMS',
      signin: 'Login',
      email: 'Email',
      password: 'Password',
      old_password: 'Old password',
      forgot_password: 'Forgot Password',
      register: 'Register Account ',
      remember_me: 'Remember me',
      email_required: 'Email is required',
      password_required:'Password is required',
      email_error:'This is not email type',
      password_min:'Password has least 6 characters',
      messages:{
        success_login: 'Login scuccess',
        error_login: 'Login fail'
      }
    },

    dashboard: {
      title: 'Dashboard',
      desc: 'Description',
      hello: 'Hello',
      project: 'Project Dashboard',
      my_dashboard:'My Dashboard',
      ticket_chart: 'Actual work Chart',
      project_cost: 'Project - Hours',
      category_ticket: 'Category chart',
      category_process: 'Process chart',
      process_ticket: 'Process - Ticket',
      member_cost: 'Member - Hours',
      company_dashboard: 'Company Dashboard',
      ListProject:'List project',
      dataMatching: "Data not matching "
    },

    menus: {
      project: 'Projects Management',
      dashboard: 'Dashboard',
      reports: 'Reports Management',
      missing_reports: 'Missing Reports',
      users: 'Users Management',
      settings: 'Settings Management',
      overtime: 'Overtime Management',
      overtime_report: 'Overtime Reports', 
      vacation: 'Absence Management',
      vacation_report: 'Absence Reports'

    },
    change_password: {
      change_title: 'Change Password',
      new_password: 'New Password',
      change_button: 'Change',
      cancel: 'Cancel',
      messages:{
        success_change: 'Change Password successful',
        error_change: 'Change Password fail'
      }
    },

    register: {
      login_account: 'Login',
      login: 'I had account',
      register_title: 'Register new account',
      register_submit: 'Register',
      answer_required: 'Please type your answer',
      name_required: 'Please type name',
      new_password: 'Email has send. Please checked email !',
      register_success: 'Register successful',
      register_error: 'Register fail'
    },

    forgot: {
      forgot_submit: 'Send',
      forgot_title: 'Get Password'
    },

    services: {
      user: {
        getQuestions: {
          fail: 'get questions fail',
          success: 'get questions success'
        }
      }
    },

    header: {
      logout: 'Logout'
    },

    report: {
      title: 'Reports',
      search: 'Search',
      input: 'Daily Report',
      missing: 'Missing Report',
      daily: 'Daily report',
      member: 'Member',
      dayoff: 'Day-off (days)',
      day_off: 'Day-Off',
      for_got:'Forgot',
      vacation: 'Vacation',
      forgot: 'Forgot (days)',
      choose: 'Choose Month:',
      current: 'This Month:',
      date: 'Date',
      from: 'From',
      to : 'To',
      hours: 'Hours',
      project: 'Project',
      ticket: 'Ticket',
      category: 'Category',
      process: 'Process',
      note: 'Note',
      operation: 'Operation',
      total: 'Total:',
      day: 'Day',
      reason: 'Reason',
      clear: 'Clear',
      at: 'at month',
      type: 'Status',
      accept:'Accept', 
      reject: 'Reject',
      accept_total: 'Accepted Time Total: ',
      work_off: 'Work Off',
      weekend_time: 'Weekend Time: ', 
      daily_time: 'Daily Time: ',
      show: 'Number of reports per page : ',
      messages:{
        error_delete: 'Delete fail',
        error_search: 'Search fail'
      }

    },

    project:{
      detail: 'Detail',
      search: 'Search',
      index: 'Index',
      id: 'ID',
      deleteAction: 'Delete',
      name: 'Name',
      clear: 'Clear',
      desc:'List',
      search_list:'Search Projects',
      agree: 'Agree',
      add_new: 'Add New Project',
      users: 'No. of Members',
      add_comfirm:'Add',
      close:'Close',
      save_change:'Save change',
      add_user:'Add User',
      greater: 'Plan Time must be smaller than or equal Quotation Time',
      disable: 'Disable Projects',
      enable: 'Enable Projects',
      quotation: 'Quotation Time (hours)',
      plan: 'Plan Time (hours)',
      messages:{
        error: 'Fail action',
        error_search: 'Search fail'
      },
      add: {
        success: 'Success. Please choose users for project',
        please_add: 'Please type your project name',
        add_user: 'Add user to project',
        please_choose:'You must be choose at least one user !'
      },
      update: {
        success: 'Update successful',
      },
      delete: {
        confirm: 'Do you want delete user with id: '
      },
      remove: {
        success: 'Remove Success'
      },
      edit: {
        title: 'Edit Project',
        not_data: 'Do not have data'
      },
    },
    users: {
      list: 'List Users',
      lock: 'Lock',
      unlock: 'Unlock',
      name: 'User Name',
      edit: 'Edit User',
      match: 'Password does not match',
      form_add: ' Form to add new user',
      profile: 'Profile User',
      text: 'Search Text'
    },

    setting: {
      category_management: 'Category Management', 
      process_management: 'Process Management', 
      create_setting: 'Create Setting',
      type: 'Type',

    },
  };

  $translateProvider.translations('en', translator);

});
