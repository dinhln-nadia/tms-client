'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ReportMissingCtrl
 * @description
 * # ReportMissingCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('ReportMissingCtrl', function ($scope, $rootScope, $localStorage, $filter, $state, commonService, ENV) {
  	$scope.reportsPerPage = 10;
  	$scope.reports = [];
  	$scope.currentPage = 1;
  	$scope.data_search = '';
    $scope.users = [];
    $scope.months = [{'id' :1,'name' : 'January'},{'id' :2,'name' : 'February'},{'id' :3,'name' : 'March'},{'id' :4,'name' : 'April'},
    {'id' :5,'name' : 'May'},{'id' :6,'name' : 'June'},{'id' :7,'name' : 'July'},{'id' :8,'name' : 'August'},
    {'id' :9,'name' : 'September'},{'id' :10,'name' : 'October'},{'id' :11,'name' : 'November'},{'id' :12,'name' : 'December'}];
    $scope.getmonth = '';

  	/**
  	 * get first data
  	 * @return Void
  	 */
  	$scope.initMissingReports = function () {
      $scope.$storage = $localStorage;
      if($localStorage.premission != 1 )
      {
        $state.go('app.report_missing_user');
      }
  		$rootScope.pageHeader = $filter('translate')('report.title');
      $rootScope.pageDesc   = $filter('translate')('report.missing');
      $rootScope.menuActive = 'missing_reports';
      $scope.getListReports($scope.currentPage);
  	};

  	/**
  	 * get List missing report
  	 * @param  page_current
  	 * @return Void
  	 */
  	$scope.getListReports = function (page_id) {
      $scope.isLoading = true;
    commonService.requestAPI('GET', ENV.host + '/reports/missing?page='+ page_id + $scope.data_search, {},
      function(data){
        $scope.isLoading = false;
        $scope.reports = data;
        $scope.data_search = '';
      });
  	};

     /**
     * set name as id of users
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data) {
      for (var i = 0; i < data.length; i++) {
        if(data[i].deleted_at != null){
          $scope.isDeleted = true;
        } else {
          $scope.isDeleted = false;
        }
        if(data[i].id == id){
          return data[i].name;
        }
      }
    };

  	/**
  	 * Search Missing Reports
  	 * @param  start_time
  	 * @param  end_time
  	 * @return void
  	 */
  	$scope.onSearchMissing = function () {
      $scope.currentPage = 1;
  		if ($scope.getmonth) {
        $scope.data_search = '&month='+ $scope.getmonth;
      }
  		$scope.getListReports($scope.currentPage);
  	};

  	/**
  	 * Export CSV
  	 * @return Void
  	 */
  	$scope.onExport = function () {

  	};
  	/**
     * get previous page
     * @return previous page
     */
    $scope.prevPage = function() {
    if ($scope.currentPage > 1) {
      $scope.currentPage--;
      $scope.getListReports($scope.currentPage);
    }
    };

    /**
     *  disabled previous page if the index of current page = 1
     */
    $scope.prevPageDisabled = function() {
      return $scope.currentPage === 1 ? "disabled" : '';
    };

    /**
     * get next page
     * @return next page
     */
    $scope.nextPage = function() {
      if($scope.reports.length == $scope.reportsPerPage){
        $scope.currentPage++;
        $scope.getListReports($scope.currentPage);
      }
    };

    /**
     * disabled next page if the index of current page is last page
     */
    $scope.nextPageDisabled = function() {
      return $scope.reports.length < $scope.reportsPerPage ? "disabled" : '';
    };

  });
