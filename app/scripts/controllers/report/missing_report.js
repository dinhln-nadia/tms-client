'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ReportMissingReportCtrl
 * @description
 * # ReportMissingReportCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
.controller('ReportMissingReportCtrl', function ($scope, $rootScope, $filter, commonService, $stateParams, ENV) {

    /**
     * using to get init data
     * @return Void
     */
    $scope.initUserMissingReports = function () {
        $rootScope.menuActive = 'missing_reports';
        if($scope.month == $stateParams.month){
            $scope.isCurrent = true;
        } else {
            $scope.isBefore = true;

        }
    };

    /**
     * check day off
     * @param  day
     * @return void
     */
     $scope.onDayOff = function (month,day,year)
    	{
        $scope.isLoading = true;
    	commonService.requestAPI('POST',ENV.host + '/reports/missing/day_off' , {day_off: day, year: year, month: month,},
        function(data){
        $scope.isLoading = false;
        $scope.getListMissing();
      });
    };

});
