'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ReportSearchCtrl
 * @description
 * # ReportSearchCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')

  .controller('ReportSearchCtrl', function ($scope, $rootScope, $localStorage, $timeout, $filter, $state, $http, commonService, ENV) {
    //search fields
    $scope.totalTimeReport = 0;
    $scope.search_project = '' ;
    $scope.search_ticket = '' ;
    $scope.search_category = '' ;
    $scope.search_process = '' ;
    $scope.search_member = '' ;
    $scope.search_type = '' ;
    $scope.start_time = '' ;
    $scope.end_time = '' ;
    //pagination
		$scope.currentPage = 1;
		$scope.reportsPerPage = 20;
    $scope.data_search = '';
    //list categories, processes, projects, users
    $scope.categories = [];
    $scope.processes = [];
    $scope.projects = [];
    $scope.users = [];
    $scope.isSearch = false;
    $scope.today = '';
    $scope.reports =[];
    $scope.isClickSearch = false;
    $scope.times =
    [
    '00:00','00:15','00:30','00:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30',
    '03:45','04:00','04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15',
    '07:30','07:45','08:00','08:15','08:30','08:45','09:00','09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00',
    '11:15','11:30','13:00','13:15','13:30','13:45','14:00','14:15','14:30','14:45',
    '15:00','15:15','15:30','15:45','16:00','16:15','16:30','16:45','17:00','17:15','17:30','17:45','18:00','18:15','18:30',
    '18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30','20:45','21:00','21:15','21:30','21:45','22:00','22:15',
    '22:30','22:45','23:00','23:15','23:30','23:45'
    ];
     $scope.types = [ {id: 0 , name: 'Daily Report'} , { id:1 , name: 'Overtime Pending'}, {id:2, name: 'Vacation Pending'},
    {id:3 , name: 'Overtime Accepted'}, {id:4 , name: 'Overtime Rejected'}, {id:5 , name: 'Vacation Accepted'} ,{id:6 , name: 'Vacation Rejected'}];
    /**
    * Intialize function for first data
    * @return Void
    */
    $scope.initSearchReports = function () {
      $rootScope.pageHeader = $filter('translate')('report.title');
      $rootScope.pageDesc   = $filter('translate')('report.search');
      $scope.$storage = $localStorage;
      $scope.getListReports($scope.currentPage);
    };

    /**
     * set name as id of category/process/project/user
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data, user) {
      if(user){
        if($localStorage.user.id != id){
          $scope.isNotUser = true;
        } else {
          $scope.isNotUser = false;
        }
      }
      if(data){
        for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){
          return data[i].name;
        }
      }
    }

      return null;
      
    };

  
    /**
     * get list report
     * @return Void
     */
    $scope.getListReports = function(page_id) {
      $scope.totalTimeReport =0;
      $scope.isLoading = true;
      var url = '';
      if($localStorage.premission !=1){
        url = ENV.host + '/reports/search?'+  'page=' + page_id + '&count=' + $scope.reportsPerPage + '&' + $scope.data_search + '&user_id=' + $localStorage.user.id ;
      } else {
        url = ENV.host + '/reports/search?'+  'page=' + page_id + '&count=' + $scope.reportsPerPage + '&' + $scope.data_search  ;
      } 
    commonService.requestAPI('GET', url, {},
      function(data){
        angular.forEach(data, function(value, key){
            $scope.totalTimeReport = $scope.totalTimeReport + value['cost'];
        });
        $scope.reports = $scope.fillTemplate (data);
        $scope.isSearch = false ;
        $scope.isLoading = false;
        $scope.isClickSearch = false;
        $scope.totalTimeReport = ($scope.totalTimeReport/60).toFixed(2)
      });
    };

   $scope.fillTemplate = function (data) {
      data.forEach (function(item){
      $scope.fillItem(item);
    });
      return data;
    };

    /**
     * Hide notification
     * @return Void
     */
    $scope.timeOut = function () {
      $timeout(function() { 
             $scope.isMessages = false;
          },1000);
    };

      
    /**
     * get time date
     * @param  start_time
     * @return date time and check over 5/nex month and different user
     */
    $scope.getTime = function (time) {
      var y = parseInt(time.substr(0,4));
      var m = parseInt(time.substr(5,2));
      if(((y == $scope.year) &&( m == $scope.month))||((y == $scope.year) && (m == $scope.month -1) && ($scope.date <=5))) {
        $scope.isOverMonth = false;
      } else {
         $scope.isOverMonth = true;
      }

      return time ;
    };

     /**
     * change format date (UTC)
     * @param  date
     * @return date
     */
    $scope.getFormatTime = function(time, day) {
      var month=[];
      month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
      var day = day.toString();
      var y = day.substr(0,4);
      var m = parseInt(day.substr(5,2)) -1;
      var d = day.substr(8,2);
      var h = time.substr(0,2);
      var i = time.substr(3,2);
      var s = '00';
      var date = y+'-'+month[m]+'-'+d+' '+h+':'+i+':00.000';
      return date;
    };

    /**
     * Save User 
     */
    $scope.SaveUser = function (data, id, report) {
      data['type'] = report['type'];
      $scope.messages = [];
      var isTwentyfour = $scope.validateReport(data,report);
      if($scope.messages.length == 0){
        if(isTwentyfour){
            data.end_time = '23:59';
          }
         $scope.isMessages = false;
        var data = $scope.convertStartEnd(data,report);
          $scope.UpdateReport(data,id,report);
       } else {
        $scope.isMessages = true;
        $scope.colorShowMessages = true;
       }  

        return 'error';    
    };

    /**
     * Validate input 
     * @param  data
     * @return value check time is 24:00
     */
    $scope.validateReport = function (data, report) {
      var nullStart = commonService.checkNull(data.start_time, 'Start time');
      if(nullStart !=0){
        $scope.messages.push(nullStart);
      }
      var isStartTime = commonService.checkTime(data.start_time, 'Start time');
      if(isStartTime !=0){
        $scope.messages.push(isStartTime);
      }
      var nullEnd = commonService.checkNull(data.end_time , 'End time');
      if(nullEnd !=0){
        $scope.messages.push(nullEnd);
      }
      if(data.end_time){
        var isTwentyfour = commonService.checkEndDay(data.end_time);
      }
      if (!isTwentyfour){
        var isEndTime = commonService.checkTime(data.end_time, 'End time');
        if(isEndTime!=0){
          $scope.messages.push(isEndTime);
        }
      }
      if($scope.messages.length == 0){
      $scope.checkGreater(data.start_time, data.end_time,report);
      }
      if(data.ticket){
      var isTicket = commonService.checkTicket(data.ticket);
      if(isTicket !=0){
        $scope.messages.push(isTicket);
        }
      }
      var nullProject = commonService.checkNull(data.project_id, 'Project');
      if(nullProject !=0){
        $scope.messages.push(nullProject);
      }
      var nullProcess = commonService.checkNull(data.process_id,'Process');
      if(nullProcess != 0){
        $scope.messages.push(nullProcess);
      }
      var nullCategory = commonService.checkNull(data.category_id , 'Category');
      if(nullCategory !=0){
         $scope.messages.push(nullCategory);
      }
      var nullNote = commonService.checkNull(data.note , 'Note');
      if(nullNote !=0){    
      $scope.messages.push(nullNote);
      }
     return isTwentyfour;
    };
      /**
     * check end_time > start_time
     * @param start_time field
     * @param  end_time field
     * @return true/false
     */
    $scope.checkGreater = function (start_time, end_time, report) {
      start_time = $scope.getFormatTime(start_time,report.date);
      end_time = $scope.getFormatTime(end_time,report.date);
      if(start_time >= end_time){
       $scope.messages.push('End time must be greater than Start time');
      } 
    };

    /**
     * 
     * Convert Start time and end time (ISO)
     * @param  data
     * @return data
     */
    $scope.convertStartEnd = function (data, report) {
      var start_time = data.start_time;
      var end_time = data.end_time;
      start_time = $scope.getFormatTime(start_time,report.date);
      end_time = $scope.getFormatTime(end_time,report.date);
      angular.extend(data,{start_time: start_time, end_time: end_time });

      return data;
    };

     /**
     * click close notification
     * @return Void
     */
    $scope.deleteMessage = function () {
      $scope.messages = [];
      $scope.isMessages = false;
    };

    /**
     * Hide notification
     * @return Void
     */
    $scope.timeOut = function () {
      $timeout(function() { 
             $scope.isMessages = false;
              $scope.messages = [];
          },1000);

    };
    /**
     * Update report row
     * @param  data
     * @param  report_id
     * @return Void
     */
    $scope.UpdateReport = function (data, id, report) {
       $scope.isLoading = true;
      // Save changes to server
       $http({
        method: 'PUT',
        url: ENV.host + '/reports/' + id, 
        data: data,
      }).success(function (response, status){
        $scope.isLoading = false;
        $scope.messages.push(response.status);
        $scope.isMessages = true;
        $scope.colorShowMessages = false;
        $scope.timeOut();
        var data = response.data;
        $scope.fillItem(data);
        $scope.reports =commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
      }).error(function (response, status) {
         $scope.isLoading = false;
        $scope.fillItem(data);
        if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
          $scope.colorShowMessages = true;
        }
        commonService.timeoutToken(status);
      });
    
    };
   
     /**
     * change start_time and end_time to 00:00 format
     * @param  data (start_time and end_time ICT format)
     * @return data
     */
    $scope.fillItem = function (data) {
      data['date'] = data.start_time.substr(0,10);
      data['start_time'] = data.start_time.substr(11,5);
      data['end_time']= data.end_time.substr(11,5);
      var isTwentyThree = commonService.checkNearEndDay(data['end_time']);
        if(isTwentyThree){
           data['end_time']  = '24:00';
        }

      return data;
      };
      

    /**
     * Intialize function for delete button
     * @param  int id
     * @return Void
     */
    $scope.onDelete = function (report) {
       $scope.isLoading = true;
      commonService.requestAPI('DELETE', ENV.host + '/reports/' + report.id, {},
        function(data){
          $scope.isLoading = false;
          $scope.reports =commonService.spliceRow(report, $scope.reports);
      });
    };

    /**
     * clear search fields
     * @return Void
     */
    $scope.onClear = function () {
      $scope.start_time = '' ;
      $scope.end_time = '';
      $scope.search_project = '';
      $scope.search_member = '';
      $scope.search_ticket = '';
      $scope.search_process = '';
      $scope.search_category = '';
      $scope.search_type = '' ;
      $scope.data_search = '';
      $scope.hasStartEnd = false;
      $scope.getListReports($scope.currentPage);
    };
    /**
     * cancel button when edit
     * @param   row form 
     * @return Void
     * 
     */
    $scope.isCancel = function (rowform) {
      rowform.$cancel();
      if($scope.messages){
        $scope.timeOut();
      }
    };

   /**
    * change minutes to hours
    * @param  cost
    * @return Void
    */
    $scope.getCost = function (cost) {

       return (cost/60).toFixed(2);
    };

    /**
     * click button search
     * @return void
     */
    $scope.onClickSearch = function () {
      $scope.isClickSearch = true;
      $scope.onSearchReport();
    };

    /**
     * Intialize function for search button
     * @return list reports
     */
    $scope.onSearchReport= function (start, end , data) {
      if(data){
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      $scope.search_project = data['search_project'];
      $scope.search_member = data['search_member'];
      $scope.search_ticket = data['search_ticket'];
      $scope.search_process = data['search_process'];
      $scope.search_category = data['search_category'];
      $scope.key_note = data['key_note'];
      $scope.search_type = data['search_type'];
      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      $scope.data_search = '';
      if($scope.search_project){
        $scope.data_search = 'project_id=' + $scope.search_project + '&';
      }
      if ($scope.search_member){
        $scope.data_search  += 'user_id='+ $scope.search_member + '&';
      }
      if ($scope.key_note){
        $scope.data_search  += 'key_note='+ $scope.key_note + '&';
      }
      if ($scope.search_ticket) {
         $scope.data_search += 'ticket=' + $scope.search_ticket + '&';
      }
      if($scope.search_process) {
        $scope.data_search += 'process_id='+ $scope.search_process + '&';
      }
      if ($scope.search_category) {
        $scope.data_search += 'category_id=' + $scope.search_category + '&';
      }
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.search_type != "undefined" && $scope.search_type != null && $scope.search_type !== ''){
         $scope.data_search += 'type=' + $scope.search_type + '&';
      }
      $scope.isSearch = true ;
      if($scope.isClickSearch){
        $scope.currentPage = 1;
        $scope.getListReports($scope.currentPage);
      }
    };

    /**
     * get previous page
     * @return previous page
     */
    $scope.prevPage = function() {
    if ($scope.currentPage > 1) {
      $scope.currentPage--;
      $scope.getListReports($scope.currentPage);
    }
    };

    /**
     *  disabled previous page if the index of current page = 1
     */
    $scope.prevPageDisabled = function() {

      return $scope.currentPage === 1 ? "disabled" : '';
    };

    /**
     * get next page
     * @return next page
     */
    $scope.nextPage = function() {
      if($scope.reports.length == $scope.reportsPerPage){
        $scope.currentPage++;
        $scope.getListReports($scope.currentPage);
      }
    };

    /**
     * disabled next page if the index of current page is last page
     */
    $scope.nextPageDisabled = function() {

      return $scope.reports.length < $scope.reportsPerPage ? "disabled" : '';
    };

});
