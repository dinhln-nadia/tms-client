'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ReportMainCtrl
 * @description
 * # ReportMainCtrl
 * Controller of the TMSApp
 */

var app = angular.module('TMSApp');



app.controller('ReportMainCtrl', function ($scope, $window, $rootScope, $filter,$timeout, $stateParams, $localStorage, $http,commonService, ENV) {

  if($localStorage.premission == 1) {
    $scope.showSearch = true;
  } else {
    $scope.showSearch = false;
  }
  $scope.reports = [];
  $scope.oldDay = '';
  $scope.isAdd = false;
  $scope.isNew = false;
  $scope.option = 0;
  $scope.isDayoff = false;
  $scope.times =
    [
    '00:00','00:15','00:30','00:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30',
    '03:45','04:00','04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15',
    '07:30','07:45','08:00','08:15','08:30','08:45','09:00','09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00',
    '11:15','11:30','13:00','13:15','13:30','13:45','14:00','14:15','14:30','14:45',
    '15:00','15:15','15:30','15:45','16:00','16:15','16:30','16:45','17:00','17:15','17:30','17:45','18:00','18:15','18:30',
    '18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30','20:45','21:00','21:15','21:30','21:45','22:00','22:15',
    '22:30','22:45','23:00','23:15','23:30','23:45','24:00'
    ];

  /**
   * Init data for add new report
   *
   * @return void
   */
  $scope.initReport = function () {
    $rootScope.pageHeader = $filter('translate')('report.title');
    $rootScope.pageDesc   = $filter('translate')('report.daily');
    $scope.$storage = $localStorage;
    $rootScope.menuActive = 'reports';
    $scope.typeReports = 0;
    if($stateParams.day){
      $scope.typeReports = 0;
      var date = new Date(Date.UTC($scope.year,$stateParams.month-1,$stateParams.day));
      $scope.isCheckedMissing = true;
      $scope.getListReports(date);
    } else {
      $scope.isCheckedMissing = false;
      $scope.getListReports();
    }

    $scope.localMissing = $localStorage;

  };
  /**
   * Get data reports
   *
   * @return void
   */
  $scope.getListReports = function (day) {
    var url;
    $scope.isLoading = true;
    $scope.isMessages = false;
    $scope.limit_row = 2;
    var start_time;
    var end_time;
    if(!day){
    $scope.isOldDay = false;
    start_time = $scope.getStartTime();
    end_time = $scope.getEndTime();
    } else {
      if(!$scope.isCheckedMissing){
       day = new Date(Date.UTC(day.getFullYear(),day.getMonth(),day.getDate()));
      } else {
        $scope.isCheckedMissing = false;
      }
      start_time = $scope.getStartTime(day);
      end_time = $scope.getEndTime(day);
      $scope.isOldDay = true;
      $scope.oldDay = start_time;
    }
    $scope.dayoffDay = start_time;
     url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&user_id=' + $localStorage.user.id + '&type=' + $scope.typeReports;
    commonService.requestAPI('GET', url, {},
      function(data){
        $scope.isAdd = true;
        $scope.isLoading = false;
        $scope.colorShowMessages = false;
        $stateParams.day= '';
        if(data){
          $scope.total = data.length;
          var total = 0;
          data.forEach(function(entry) {
              total = total + parseInt(entry.cost);
          });
          $scope.reports = $scope.fillTemplate (data);
          $scope.total_report = (total/60).toFixed(2);
        }
      });

  };

  /**
   * add meeting morning
   *
   */
   $scope.AddMeeting  = function () {

    $scope.dataMeeting = {
      start_time: "08:00",
      end_time: "08:30",
      process_id:  45,
      project_id: 22,
      category_id: 39,
      ticket: "",
      type: 0,
      note: "Họp buổi sáng."
    };
    $scope.type = {id: "add_new", cost: "", note: "", ticket: ""}
    $scope.SaveUser ($scope.dataMeeting, 'add_new',$scope.type);

   }
  /**
   * Change start_time and end_time from date to time
   * @param  data from server
   * @return data after change
   */
  $scope.fillTemplate = function (data) {
    data.forEach (function(item){
     $scope.fillItem(item);
    });

    return data;
  };


  /**
   * get Start time
   * @param  date
   * @return start_time
   */
  $scope.getStartTime = function (day){
    var start_time;
    if(!day){
        start_time = new Date();
    } else  {
      start_time = day;
    }
    start_time = start_time.toISOString();
    start_time = start_time.substr(0,10) + ' '+ '00:00:00.000';
    return start_time;
  };

  /**
   * get End time
   * @param  date
   * @return  end_time
   */
  $scope.getEndTime = function (day) {
    var end_time;
    if(!day){
        end_time = new Date();
    } else  {
      end_time = day;
    }
    end_time = end_time.toISOString();
    end_time = end_time.substr(0,10)+ ' '+'23:59:00.000';
    return end_time;

  };


  /**
    * change minutes to hours
    * @param  cost
    * @return Void
    */
  $scope.getCost = function (cost) {

     return (cost/60).toFixed(2);
  };

  /**
   * Get datetime for today
   *
   * @return void
   */
  $scope.getDateTime = function() {
      if(!$scope.isOldDay){
      var today = new Date();
      var d = today.getDate();
      var m = today.getMonth()+ 1; //January is 0!
      var y = today.getFullYear();
      d = commonService.insertStringZero(d);
      m = commonService.insertStringZero(m);
    } else {
      var m = $scope.oldDay.substr(5,2);
      var y = $scope.oldDay.substr(0,4);
      var d = $scope.oldDay.substr(8,2);
    }
     if(((y == $scope.year) &&( m == $scope.month))||((y == $scope.year) && (m == $scope.month -1) && ($scope.date <=5))) {
        $scope.isOverMonth = false;
      } else {
         $scope.isOverMonth = true;
      }

     return d+'/'+ m +'/'+y;
  };

  /**
   * change format date (UTC)
   * @param  date
   * @return date
   */
  $scope.getFormatTime = function(time, statusOld) {
    var m;
    var d;
    var y;
    var month;
    month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    if(statusOld || $scope.isCheckedMissing){
      m = parseInt($scope.oldDay.substr(5,2)) -1;
      y = $scope.oldDay.substr(0,4);
      d = $scope.oldDay.substr(8,2);
    } else {
      var today = new Date();
       m = today.getMonth();
       d = today.getDate();
       y = today.getFullYear();
       d = commonService.insertStringZero(d);
     }
      time = time.toString();
      var h = time.substr(0,2);
      var i = time.substr(3,2);
      var s = '00';
      var date = y+'-'+month[m]+'-'+d+' '+h+':'+i+':00.000';

      return date;

  };

  /**
   * Save User
   *
   * @return void
   */
  $scope.SaveUser = function (data, id, report) {
    $scope.messages = [];
    var isTwentyfour = $scope.validateReport(data);
    if($scope.messages.length == 0){
      if(isTwentyfour){
          data.end_time = '23:59';
        }
      $scope.isMessages = false;
      var data = $scope.convertStartEnd(data);
      if(id == 'add_new'){
       data['type'] = $scope.typeReports;
        $scope.CreateReport(data, report);
      } else {
         data['type'] = report['type'];
        $scope.UpdateReport(data,report,id);
      }

      $localStorage.CheckMissingReport = true;

    } else {
      $scope.isMessages = true;
      $scope.colorShowMessages = true;
     }

     return "error";
  };
  /**
     * Validate input
     * @param  data
     * @return value check time is 24:00
     */
  $scope.validateReport = function (data) {
    var nullStart = commonService.checkNull(data.start_time, 'Start time');
    if(nullStart !=0){
      $scope.messages.push(nullStart);
    }
    var isStartTime = commonService.checkTime(data.start_time, 'Start time');
    if(isStartTime !=0){
      $scope.messages.push(isStartTime);
    }
    var nullEnd = commonService.checkNull(data.end_time , 'End time');
    if(nullEnd !=0){
      $scope.messages.push(nullEnd);
    }
    if(data.end_time){
      var isTwentyfour = commonService.checkEndDay(data.end_time);
    }
    if (!isTwentyfour){
     var isEndTime = commonService.checkTime(data.end_time, 'End time');
        if(isEndTime!=0){
          $scope.messages.push(isEndTime);
        }
    }
    if($scope.messages.length == 0){
    $scope.checkGreater(data.start_time, data.end_time);
    }
    if(data.ticket){
      var isTicket = commonService.checkTicket(data.ticket);
    if(isTicket !=0){
      $scope.messages.push(isTicket);
      }
    }
    var nullProject = commonService.checkNull(data.project_id, 'Project');
    if(nullProject !=0){
      $scope.messages.push(nullProject);
    }
    var nullProcess = commonService.checkNull(data.process_id,'Process');
    if(nullProcess != 0){
      $scope.messages.push(nullProcess);
    }
    var nullCategory = commonService.checkNull(data.category_id , 'Category');
    if(nullCategory !=0){
       $scope.messages.push(nullCategory);
    }
    var nullNote = commonService.checkNull(data.note , 'Note');
    if(nullNote !=0){
    $scope.messages.push(nullNote);
    }
    return isTwentyfour;
  };
  /**
   * check end_time > start_time
   * @param start_time field
   * @param  end_time field
   * @return true/false
   */
  $scope.checkGreater = function (start_time, end_time) {
        start_time = $scope.getFormatTime(start_time,$scope.isOldDay);
        end_time = $scope.getFormatTime(end_time,$scope.isOldDay);
      if(start_time >= end_time){
        $scope.messages.push('End time must be greater than Start time');
      }
  };

  /**
   *
   * Convert Start time and end time (ISO)
   * @param  data
   * @return data
   */
  $scope.convertStartEnd = function (data) {
    var start_time = data.start_time;
    var end_time = data.end_time;
    start_time = $scope.getFormatTime(start_time,$scope.isOldDay);
    end_time = $scope.getFormatTime(end_time,$scope.isOldDay);
    angular.extend(data,{start_time: start_time, end_time: end_time });
    return data;

  };

  /**
   * edit button
   * @param  row form
   * @return void
   */
  $scope.isEdit = function (rowform) {
    rowform.$show();
    $scope.isAdd = false;
  };

  /**
   * cancel button when edit
   * @param  rowform
   * @return Void
   */
  $scope.isCancel = function (rowform) {
    rowform.$cancel();
    $scope.isAdd = true;
    if($scope.messages){
        $scope.timeOut();
     }
  };

  $scope.deleteNoitify = function (day,monthInstall, thisMonth) {

    if( monthInstall == thisMonth) {
      var index = $scope.localMissing.dataMissing.map(function(d) { return d['day']; }).indexOf(day)
      if(index == -1) {

       return false;

      }
      $scope.localMissing.dataMissing.splice(index, 1);

    } else {
      var index = $scope.localMissing.dataMissing1.map(function(d) { return d['day']; }).indexOf(day)
      if(index == -1) {

       return false;

      }

      $scope.localMissing.dataMissing1.splice(index, 1);

    }

  }

  $scope.pushNoitify = function (day,monthInstall, thisMonth) {



    if( monthInstall == thisMonth) {
      var index = $scope.localMissing.dataMissing.map(function(d) { return d['day']; }).indexOf(day)
      if (index != -1) {
        return false;
      }
      $scope.localMissing.dataMissing.push({'day':day, 'status':0 });

    } else {
      var index = $scope.localMissing.dataMissing1.map(function(d) { return d['day']; }).indexOf(day)
      if (index != -1) {
        return false;
      }
      $scope.localMissing.dataMissing1.push({'day':day, 'status':0 });

    }
  }
 /**
  * Create new report
  * @param data and report index
  * @param Void
  */
  $scope.CreateReport = function (data, report) {
     $scope.isLoading = true;
    var cost = report.cost;
      $http({
        method:'POST',
        url: ENV.host + '/reports',
        data:data,
      }).success(function (response, status){
        console.log(response);
        var checkDate = new Date();
        if (response.dayMissing.status != 1) {
          if(response.dayMissing.status != 0) {

            $scope.deleteNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

          } else {

            $scope.pushNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

          }
        }
        $scope.isLoading = false;
        $scope.colorShowMessages = false;
        $scope.messages.push(response.status);
        if($scope.messages){
          $scope.isMessages = true;
          $scope.timeOut();
        }
        $scope.isAdd = true;
        var data = response.data;
        $scope.fillItem(data);
        if(report.$$hashKey) {
          $scope.reports = commonService.spliceRow(report, $scope.reports);
        }
        $scope.reports.unshift(data);
        $scope.isNew = false;
        $scope.limit_row ++;
        if(data){
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60)).toFixed(2);
      }
      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.colorShowMessages = true;
        $scope.fillItem(data);
        if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
        }
        $scope.isNew = true;
        commonService.timeoutToken(status);
      });
  };
 /**
  * Update Report
  * @param data and report index
  * @param Void
  */
  $scope.UpdateReport = function (data, report, id) {
    $scope.isLoading = true;
     var cost = report.cost;
    $http({
        method: 'PUT',
        url: ENV.host + '/reports/' + id
        ,
        data: data,
      }).success(function (response, status){

        var checkDate = new Date();

        if (response.dayMissing.status != 1) {
          if(response.dayMissing.status != 0) {
            $scope.deleteNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);
          } else {
            $scope.pushNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);
          }
        }

        $scope.messages.push(response.status);
        $scope.isLoading = false;
        $scope.isAdd = true;
        if($scope.messages){
          $scope.isMessages = true;
          $scope.colorShowMessages =false;
          $scope.timeOut();
        }
        var data = response.data;
        $scope.fillItem(data);
        if(data){
        $scope.reports = commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60) - parseFloat(cost/60)).toFixed(2);
        }

      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.colorShowMessages = true;
        $scope.fillItem(data);
         if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
        }

        $scope.isNew = false;
        commonService.timeoutToken(status);
      });
    };

  /**
   * change start_time and end_time to 00:00 format
   * @param  data (start_time and end_time ICT format)
   * @return data
   */
  $scope.fillItem = function (data) {
    data['start_time'] = data.start_time.substr(11,5);
    data['end_time'] = data.end_time.substr(11,5);
    var isTwentyThree = commonService.checkNearEndDay(data.end_time);
        if(isTwentyThree){
          data['end_time']  = '24:00';
        }
    return data;
    };

  /**
   * Function to handle click new button
   *
   * @return void
   */
  $scope.newRow = function () {
    if($scope.limit_row!=0){
      $scope.isAdd = true;
      $scope.inserted = {
      id: 'add_new',
      cost: '',
      note: '',
      ticket: ''
    };
    $scope.reports.unshift($scope.inserted);
    $scope.limit_row --;
    $scope.isNew = true;
    } else {
       $scope.isAdd = false;
    }
  };


  /**
   * duplicate row.
   * @param data old row.
   * @return void
   */
  $scope.duplicateRow = function (data) {
    if($scope.limit_row!=0){
      $scope.isAdd = true;
      $scope.inserted = {
      category_id:data.category_id,
      id: 'add_new',
      project_id: data.project_id,
      process_id: data.process_id,
      ticket: data.ticket,
      note: data.note,
      type: data.type
    };
    $scope.reports.unshift($scope.inserted);
    $scope.limit_row --;
    $scope.isNew = true;
    } else {
       $scope.isAdd = false;
    }
  };



  $scope.onDayoff = function () {
    $scope.isDayoff = true;
  };

  $scope.dayOff = function () {
    $scope.messages = [];
    var year = parseInt($scope.dayoffDay.substr(0,4));
    var month = parseInt($scope.dayoffDay.substr(5,2));
    var day = parseInt( $scope.dayoffDay.substr(8,2));
      $scope.isLoading = true;
      $http({
        method: 'POST',
        url:ENV.host + '/reports/missing/day_off',
        data: {
          day_off: day,
          year: year,
          month: month,
        },
      }).success(function (response, status){
        $scope.isLoading = false;
        $scope.colorShowMessages = false;
        $scope.isDayoff = false;
        $scope.isAdd = false;
        $scope.messages.push(response.status);
      }).error(function (response,status) {
        $scope.isLoading = false;
        $scope.colorShowMessages = true;
        $scope.isDayoff = false;
        $scope.messages.push(response.error);
        commonService.timeoutToken(status);
      });
      if($scope.messages){
          $scope.isMessages = true;
          $scope.timeOut();
        }
  };
  /**
   * cancel dayoff button
   * @return Void
   */
  $scope.cancelDayoff = function () {
    $scope.isDayoff = false;
  };
  /**
   * click close notification
   * @return Void
   */
  $scope.deleteMessage = function () {
    $scope.messages = [];
    $scope.isMessages = false;
  };

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() {
           $scope.isMessages = false;
           $scope.messages = [];
        },5000);
  };

  /**
   * Delete row in local
   *
   * @return void
   */
  $scope.deleteRow = function (report) {
     $scope.isLoading = true;
    var cost = report.cost;
    commonService.requestAPI('DELETE', ENV.host + '/reports/' + report.id, {},
        function(data){
           $scope.isMessages = false;
           $scope.isLoading = false;
           $scope.colorShowMessages = false;
           $scope.reports = commonService.spliceRow(report, $scope.reports);
           $scope.total_report = (parseFloat($scope.total_report) - parseFloat(cost/60)).toFixed(2);
            var checkDate = new Date();
            if (data[0].status != 1) {
              if(data[0].status != 0) {

                $scope.deleteNoitify(data[0].day,data[0].month, checkDate.getMonth() +1);

              } else {

                $scope.pushNoitify(data[0].day,data[0].month, checkDate.getMonth() +1);

              }
            }
      });
  };

  /**
   * get out Row
   * @param  report id
   * @return Void
   */
  $scope.getOutRow = function (report) {
    if($scope.messages){
      $scope.timeOut();
    }
   $scope.reports = commonService.spliceRow(report, $scope.reports);
    $scope.limit_row ++;
    $scope.isAdd = true;
    if($scope.limit_row ==2 ) {
      $scope.isNew = false;
      $scope.isAdd = true;
    }
  };



  /**
     * set name as id of category/process/project
     * @param   id
     * @param   list
     * @return name
     */
  $scope.findName = function(id, data) {
      if(data){
        for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){

          return data[i].name;
        }
      }
    }
    return  null;

    };

  /*
   * Duplicate Day
   */
   $scope.duplicateDay = function (date) {

      $scope.isLoading = true;
      var day = date.substr(0,2);
      var month = date.substr(3,2);
      var year = date.substr(6,4);
      var dateInDay = new Date(year,month-1,day);
      var start_time =  year + '-' + month + '-' + day+ ' ' + '00:00:01';
      var end_time   =  year + '-' + month + '-' + day+ ' ' + '23:59:00';
      $scope.messages = [];
      $http({
        method: 'GET',
        url: ENV.host + '/reports/duplicate-day?'+'start_time='+ start_time + '&end_time=' + end_time,
        data: {},
      }).success(function (response, status){

        $scope.getListReports(dateInDay);

        var checkDate = new Date();

        if(response.dayMissing.status != 0) {

          $scope.deleteNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

        } else {

          $scope.pushNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

        }
      }).error(function (response,status) {
        $scope.isLoading = false;
        if(response){
          $scope.messages.push(response.error);
          $scope.colorShowMessages =true;
          $scope.isMessages = true;
          $scope.timeOut();
        }
        commonService.timeoutToken(status);
      });
   }

});
