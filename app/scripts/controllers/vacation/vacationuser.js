'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:VacationVacationuserCtrl
 * @description
 * # VacationVacationuserCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('VacationVacationuserCtrl', function ($scope, $window, $rootScope, $filter,$timeout, $stateParams, $localStorage, $http,commonService, ENV, $state) {

  $scope.reports = [];
  $scope.isAdd = false;
  $scope.isNew = false;
  $scope.isClickSearch = false;
  //get absence reports for user_id(absence pending, accepted and denied)
  $scope.option = 3;
  $scope.typeReports = 2;
  $scope.times =
    [
    '00:00','00:15','00:30','00:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30',
    '03:45','04:00','04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15',
    '07:30','07:45','08:00','08:15','08:30','08:45','09:00','09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00',
    '11:15','11:30','13:00','13:15','13:30','13:45','14:00','14:15','14:30','14:45',
    '15:00','15:15','15:30','15:45','16:00','16:15','16:30','16:45','17:00','17:15','17:30','17:45','18:00','18:15','18:30',
    '18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30','20:45','21:00','21:15','21:30','21:45','22:00','22:15',
    '22:30','22:45','23:00','23:15','23:30','23:45','24:00'
    ];
   $scope.types = [  {id:2, name: 'Vacation Pending'}, {id:5 , name: 'Vacation Accepted'} ,{id:6 , name: 'Vacation Rejected'}];

  /**
   * Init data for add new report
   *
   * @return void
   */
  $scope.initVacationReport = function () {
    $rootScope.pageHeader = $filter('translate')('report.title');
    $rootScope.pageDesc   = $filter('translate')('menus.vacation_report');
    $rootScope.menuActive = 'vacation';
    $scope.getListReports();
  };

  /**
   * Get data reports
   *
   * @return void
   */
  $scope.getListReports = function () {
    $scope.disabledButton = false;
    var url;
    $scope.isLoading = true;
    $scope.isMessages = false;
    $scope.limit_row = 2;
      var start_time;
      var end_time;
      var day = new Date();
      $scope.year = day.getFullYear();
      $scope.month = day.getMonth()+1;
      $scope.date = day.getDate();
      start_time = $scope.getStartTime();
      end_time = $scope.getEndTime();
      if($scope.data_search){
        url = ENV.host + '/reports/search?option='+  $scope.option + '&user_id=' + $localStorage.user.id +'&'+ $scope.data_search ;
      } else  {
        url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&user_id=' + $localStorage.user.id + '&option=' + $scope.option ;
      }
      $scope.today = day.toISOString().substr(0,10);
    commonService.requestAPI('GET', url, {},
      function(data){
        $scope.isAdd = true;
         $scope.isLoading = false;
        if(data){
          $scope.total = data.length;
         var total = 0;
          var accept_total = 0;
          data.forEach(function(entry) {
              total = total + parseInt(entry.cost);
              if(entry['type'] == 5){
                accept_total = accept_total + parseInt(entry.cost);
              }
          });
          $scope.reports = $scope.fillTemplate (data);
          $scope.total_report = (total/60).toFixed(2);
          $scope.accept_total = (accept_total/60).toFixed(2);
         $scope.isClickSearch = false;
        }
      });

  };

  /**
   * Change start_time and end_time from date to time
   * @param  data from server
   * @return data after change
   */
  $scope.fillTemplate = function (data) {
    data.forEach (function(item){
     $scope.fillItem(item);
    });

    return data;
  };

  /**
   * get Start time
   * @param  date
   * @return start_time
   */
  $scope.getStartTime = function (){
    var start_time;
    var start;
    start = commonService.insertStringZero($scope.month);
    start_time = $scope.year + '-' + start + '-'+ '01'+' '+'00:00:00.000';
    return start_time;
  };

  /**
   * get End time
   * @param  date
   * @return  end_time
   */
  $scope.getEndTime = function () {
    var end;
    var end_time;
    end =commonService.insertStringZero($scope.month +1);
    if($scope.month == 12)
    {
      end_time = $scope.year+1 + '-' + '01' + '-'+ '01'+' '+'00:00:00.000';
    } else {
      end_time = $scope.year + '-' + end + '-'+ '01'+' '+'00:00:00.000';
    }

    return end_time;

  };

  /**
    * change minutes to hours
    * @param  cost
    * @return Void
    */
  $scope.getCost = function (cost) {
     return (cost/60).toFixed(2);
  };
   /**
     * search on project_id and user_id
     * @return Void
     */
    $scope.onClickSearch = function () {
      $scope.isClickSearch = true;
      $scope.onSearchAbsence();
    };

  /**
     * clear search fields
     * @return Void
     */
    $scope.onClear = function () {
      $scope.start_time = '';
      $scope.end_time = '';
      $scope.search_type = '';
      $scope.data_search = '';
      $scope.hasStartEnd = false;
      $scope.getListReports($scope.currentPage);
    };

    /**
     * Search Missing Reports
     * @param  start_time
     * @param  end_time
     * @return void
     */
    $scope.onSearchAbsence = function (start, end, data) {
       if(data){
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      $scope.search_type = data['search_type'];
      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      $scope.data_search = '';
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.search_type != "undefined" && $scope.search_type != null && $scope.search_type !== ''){
         $scope.data_search += 'type=' + $scope.search_type + '&';
      }
      $scope.isSearch = true ;
      if($scope.isClickSearch){
        $scope.currentPage = 1;
        $scope.getListReports($scope.currentPage);
      }
    };

  /**
   * change format date (UTC)
   * @param  date
   * @return date
   */
  $scope.getFormatTime = function(time ,date_vacation,report) {
    var m;
    var d;
    var y;
    var month=[];
    month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    if(date_vacation){
      var typedate = typeof(date_vacation);
      if(typedate == 'string'){
        m = parseInt(date_vacation.substr(5,2))-1;
        y = date_vacation.substr(0,4);
        d = date_vacation.substr(8,2);

      } else {
        m = date_vacation.getMonth();
        d = date_vacation.getDate();
        y = date_vacation.getFullYear();
        d = commonService.insertStringZero(d);
      }

    } else {
      m = parseInt(report.date_vacation.substr(5,2)) -1;
      y = report.date_vacation.substr(0,4);
      d = report.date_vacation.substr(8,2);
     }
      time = time.toString();
      var h = time.substr(0,2);
      var i = time.substr(3,2);
      var s = '00';
      var date = y+'-'+ month[m]+'-'+ d +' '+ h+':'+i+':00.000';
      return date;
  };

  /**
   * Save User
   *
   * @return void
   */
  $scope.SaveUser = function (data, id, report) {
  	data['type'] = $scope.typeReports;
    data['process_id'] = 2;
    data['project_id'] = 1;
    data['category_id'] = 1;
    $scope.messages = [];
    var isTwentyfour = $scope.validateReport(data, report);
    if($scope.messages.length == 0){
      if(isTwentyfour){
          data.end_time = '23:59';
        }
      $scope.isMessages = false;
      data = $scope.convertStartEnd(data,report);
       delete data['date_vacation'];
      if(id == 'add_new'){
        $scope.CreateReport(data, report);
      } else {
        data['type'] = report['type'];
        $scope.UpdateReport(data,report,id);
      }
    } else {
      $scope.isMessages = true;
      $scope.colorShowMessages =true;
     }

     return "error";
  };

  $scope.checkDateVacation = function(date_vacation, report) {
    var m = date_vacation.getMonth()+1;
    if(m < $scope.month){
      return 'You can not pick date at last month';
    }
    return 0;
  };
  /**
     * Validate input
     * @param  data
     * @return value check time is 24:00
     */
  $scope.validateReport = function (data , report) {
    if(!data.date_vacation){
    	if(!report.date_vacation){
    			$scope.messages.push('Please choose Date for vacation');
    	 }
    } else {
      var checkDate = $scope.checkDateVacation(data['date_vacation']);
      if(checkDate !=0) {
        $scope.messages.push(checkDate);
      }
    }
    var nullStart = commonService.checkNull(data.start_time, 'Start time');
    if(nullStart !=0){
      $scope.messages.push(nullStart);
    }
    var isStartTime = commonService.checkTime(data.start_time, 'Start time');
    if(isStartTime !=0){
      $scope.messages.push(isStartTime);
    }
    var nullEnd = commonService.checkNull(data.end_time , 'End time');
    if(nullEnd !=0){
      $scope.messages.push(nullEnd);
    }
    if(data.end_time){
      var isTwentyfour = commonService.checkEndDay(data.end_time);
    }
    if (!isTwentyfour){
     var isEndTime = commonService.checkTime(data.end_time, 'End time');
        if(isEndTime!=0){
          $scope.messages.push(isEndTime);
        }
    }
    if($scope.messages.length == 0){
    $scope.checkGreater(data.start_time, data.end_time, data.date_vacation ,report);
    }
    var nullNote = commonService.checkNull(data.note , 'Note');
    if(nullNote !=0){
    $scope.messages.push(nullNote);
    }
    return isTwentyfour;
  };
  /**
   * check end_time > start_time
   * @param start_time field
   * @param  end_time field
   * @return true/false
   */
  $scope.checkGreater = function (start_time, end_time, date_vacation,report) {
        start_time = $scope.getFormatTime(start_time, date_vacation,report);
        end_time = $scope.getFormatTime(end_time,date_vacation,report);
      if(start_time >= end_time){
        $scope.messages.push('End time must be greater than Start time');
      }
  };

  /**
   *
   * Convert Start time and end time (ISO)
   * @param  data
   * @return data
   */
  $scope.convertStartEnd = function (data,report) {
    var start_time = data.start_time;
    var end_time = data.end_time;
    start_time = $scope.getFormatTime(start_time,data.date_vacation,report);
    end_time = $scope.getFormatTime(end_time,data.date_vacation,report);
    angular.extend(data,{start_time: start_time, end_time: end_time });
    return data;

  };

  /**
   * edit button
   * @param  row form
   * @return void
   */
  $scope.isEdit = function (rowform) {
    rowform.$show();
    $scope.isAdd = false;
  };

  /**
   * cancel button when edit
   * @param  rowform
   * @return Void
   */
  $scope.isCancel = function (rowform) {
    rowform.$cancel();
    $scope.isAdd = true;
    if($scope.messages){
        $scope.timeOut();
     }
  };

  $scope.deleteNoitify = function (day,monthInstall, thisMonth) {

    var index = $scope.localMissing.dataMissing.map(function(d) { return d['day']; }).indexOf(day)
    if( monthInstall == thisMonth) {

      $scope.localMissing.dataMissing.splice(index, 1);

    } else {

      $scope.localMissing.dataMissing1.splice(index, 1);

    }

  }

  $scope.pushNoitify = function (day,monthInstall, thisMonth) {
    var index = $scope.localMissing.dataMissing.map(function(d) { return d['day']; }).indexOf(day)
    if (index != -1) {
      return false;
    }

    if( monthInstall == thisMonth) {
      $scope.localMissing.dataMissing.push({'day':day, 'status':0 });

    } else {
      $scope.localMissing.dataMissing1.push({'day':day, 'status':0 });

    }
  }
 /**
  * Create new report
  * @param data and report index
  * @param Void
  */
  $scope.CreateReport = function (data, report) {
     $scope.isLoading = true;
    var cost = report.cost;
      $http({
        method:'POST',
        url: ENV.host + '/reports',
        data:data,
      }).success(function (response, status){

        var checkDate = new Date();
        if (response.dayMissing.status != 1) {
          if(response.dayMissing.status != 0) {

            $scope.deleteNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

          } else {

            $scope.pushNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);

          }
        }
        $scope.isLoading = false;
        $scope.messages.push(response.status);
        if($scope.messages){
          $scope.isMessages = true;
          $scope.colorShowMessages = false;
          $scope.timeOut();
        }
        $scope.isAdd = true;
        var data = response.data;
        $scope.fillItem(data);
        $scope.reports = commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
        $scope.isNew = false;
        $scope.limit_row ++;
        if(data){
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60)).toFixed(2);
      }
      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.fillItem(data);
        if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
          $scope.colorShowMessages = true;
        }
        $scope.isNew = true;
        commonService.timeoutToken(status);
      });
  };
 /**
  * Update Report
  * @param data and report index
  * @param Void
  */
  $scope.UpdateReport = function (data, report, id) {
    $scope.isLoading = true;
     var cost = report.cost;
    $http({
        method: 'PUT',
        url: ENV.host + '/reports/' + id,
        data: data,
      }).success(function (response, status){

        var checkDate = new Date();
        if (response.dayMissing.status != 1) {
          if(response.dayMissing.status != 0) {
            $scope.deleteNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);
          } else {
            $scope.pushNoitify(response.dayMissing.day,response.dayMissing.month, checkDate.getMonth() +1);
          }
        }
        $scope.messages.push(response.status);
        $scope.isLoading = false;
        $scope.isAdd = true;
        if($scope.messages){
          $scope.isMessages = true;
          $scope.colorShowMessages = false;
          $scope.timeOut();
        }
        var data = response.data;
        $scope.fillItem(data);
        if(data){
        $scope.reports = commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60) - parseFloat(cost/60)).toFixed(2);
        }

      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.fillItem(data);
         if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
          $scope.colorShowMessages = true;
        }

        $scope.isNew = false;
        commonService.timeoutToken(status);
      });
    };

  /**
   * change start_time and end_time to 00:00 format
   * @param  data (start_time and end_time ICT format)
   * @return data
   */
  $scope.fillItem = function (data) {
  	data['date_vacation'] = data.start_time.substr(0,10);
    data['start_time'] = data.start_time.substr(11,5);
    data['end_time'] = data.end_time.substr(11,5);
    var isTwentyThree = commonService.checkNearEndDay(data.end_time);
        if(isTwentyThree){
          data['end_time']  = '24:00';
        }
    return data;
    };

  /**
   * Function to handle click new button
   *
   * @return void
   */
  $scope.newRow = function () {
   if($scope.limit_row!=0){
      $scope.isAdd = true;
      $scope.inserted = {
      id: 'add_new',
      cost: '',
      note: '',
    };
    $scope.reports.unshift($scope.inserted);
    $scope.limit_row --;
    $scope.isNew = true;
    } else {
       $scope.isAdd = false;
     }
  };

  /**
   * click close notification
   * @return Void
   */
  $scope.deleteMessage = function () {
    $scope.messages = [];
    $scope.isMessages = false;
  };

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() {
           $scope.isMessages = false;
           $scope.messages = [];
        },1000);

  };

  /**
   * Delete row in local
   *
   * @return void
   */
  $scope.deleteRow = function (report) {
     $scope.isLoading = true;
    commonService.requestAPI('DELETE', ENV.host + '/reports/' + report.id, {},
        function(data){
          $scope.isLoading = false;
          $scope.total_report = (parseFloat($scope.total_report) - parseFloat(report.cost/60)).toFixed(2);
          $scope.reports = commonService.spliceRow(report, $scope.reports);
      });
  };

  /**
     * set name as id of types
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data) {
      if(data){
        for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){
          return data[i].name;
        }
      }
    }

      return null;

    };

  /**
   * get out Row
   * @param  report id
   * @return Void
   */
  $scope.getOutRow = function (report) {
    if($scope.messages){
      $scope.timeOut();
    }
   $scope.reports = commonService.spliceRow(report, $scope.reports);
    $scope.limit_row ++;
    $scope.isAdd = true;
    if($scope.limit_row ==2 ) {
      $scope.isNew = false;
      $scope.isAdd = true;
    }
  };


});
