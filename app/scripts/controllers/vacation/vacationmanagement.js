'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:VacationVacationmanagementCtrl
 * @description
 * # VacationVacationmanagementCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('VacationVacationmanagementCtrl', function ($scope, $window, $rootScope, $filter,$timeout, $stateParams, $localStorage, $http,commonService, ENV) {

  	$scope.reportsPerPage = 20;
  	$scope.$storage = $localStorage;
  	$scope.reports = [];
  	$scope.currentPage = 1;
    //get list absence report(pending, accept and denied)
    $scope.option = 2;
  	$scope.data_search = '';
    $scope.isClickSearch = false;
    $scope.users = [];
    $scope.types = [ {id:2, name: 'Vacation Pending'}, {id:5 , name: 'Vacation Accepted'} ,{id:6 , name: 'Vacation Rejected'}];

    $scope.initVacationManagement = function () {
	    $rootScope.pageHeader = $filter('translate')('report.title');
	    $rootScope.pageDesc   = $filter('translate')('menus.vacation');
	    $rootScope.menuActive = 'vacation_management';
	    $scope.getListReports($scope.currentPage);
    };

     	/**
  	 * get List missing report
  	 * @param  page_current
  	 * @return Void
  	 */
  	$scope.getListReports = function (page_id) {
      $scope.isPagination = true;
  		var url;
  		$scope.isLoading = true;
      var start_time;
      var end_time;
      var day = new Date();
      $scope.year = day.getFullYear();
      $scope.month = day.getMonth()+1;
      start_time = $scope.getStartTime();
      end_time = $scope.getEndTime();
       if($scope.data_search){
        $scope.isPagination = false;
        url = ENV.host + '/reports/search?option='+ $scope.option +'&'+ $scope.data_search ;
      } else  {
        url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&option='+ $scope.option + '&count=' + $scope.reportsPerPage + '&page=' + page_id;
      }
    commonService.requestAPI('GET', url, {},
      function(data){
      	$scope.isLoading = false;
        if(data){
          $scope.total = data.length;
          var total = 0;
          var accept_total = 0;
          data.forEach(function(entry) {
              total = total + parseInt(entry.cost);
              if(entry['type'] == 5){
                accept_total = accept_total + parseInt(entry.cost);
              }
          });
          $scope.reports = $scope.fillTemplate (data);
          $scope.total_report = (total/60).toFixed(2);
          $scope.accept_total = (accept_total/60).toFixed(2);
         $scope.isClickSearch = false;
        }
      });
  	};
  	/**
   * Change start_time and end_time from date to time
   * @param  data from server
   * @return data after change
   */
  $scope.fillTemplate = function (data) {
    data.forEach (function(item){
     $scope.fillItem(item);
    });

    return data;
  };

   /**
    * change minutes to hours
    * @param  cost
    * @return Void
    */
  $scope.getCost = function (cost) {

     return (cost/60).toFixed(2);
  };
   /**
   * change start_time and end_time to 00:00 format
   * @param  data (start_time and end_time ICT format)
   * @return data
   */
  $scope.fillItem = function (data) {
  	data['date_vacation'] = data.start_time.substr(0,10);
    data['start_time'] = data.start_time.substr(11,5);
    data['end_time'] = data.end_time.substr(11,5);
    var isTwentyThree = commonService.checkNearEndDay(data.end_time);
        if(isTwentyThree){
          data['end_time']  = '24:00';
        }
    return data;
    };
     /**
     * set name as id of users
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data) {
       if(data){
        for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){
          return data[i].name;
        }
      }
    }

      return null;

    };


  /**
   * get Start time
   * @param  date
   * @return start_time
   */
  $scope.getStartTime = function (){
    var start_time;
    var start;
    start =  commonService.insertStringZero($scope.month);
    start_time = $scope.year + '-' + start + '-'+ '01'+' '+'00:00:00.000';

    return start_time;
  };


  /**
   * get End time
   * @param  date
   * @return  end_time
   */
  $scope.getEndTime = function () {
  	var end;
    var end_time;
    end = commonService.insertStringZero($scope.month +1);
    end_time = $scope.year + '-' + end + '-'+ '01'+' '+'00:00:00.000';

    return end_time;

  };

  	/**
  	 * Search Absence Reports
  	 * @param  start_time
  	 * @param  end_time
  	 * @return void
  	 */
  	$scope.onSearchAbsence = function (start, end, data) {
  		if(data){
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      $scope.search_member = data['search_member'];
      $scope.search_type = data['search_type'];

      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      $scope.data_search = '';
      if ($scope.search_member){
        $scope.data_search  += 'user_id='+ $scope.search_member + '&';
      }
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.search_type != "undefined" && $scope.search_type != null && $scope.search_type !== ''){
         $scope.data_search += 'type=' + $scope.search_type + '&';
      }
      $scope.isSearch = true ;
      if($scope.isClickSearch){
        $scope.currentPage = 1;
        $scope.getListReports($scope.currentPage);
      }
  	};
     /**
     * clear search fields
     * @return Void
     */
    $scope.onClear = function () {
      $scope.search_member = '';
      $scope.start_time = '';
      $scope.end_time = '';
      $scope.search_type = '';
      $scope.data_search = '';
      $scope.hasStartEnd = false;
      $scope.getListReports($scope.currentPage);
    };

    /**
     * button search
     * @return Void
     */
    $scope.onClickSearch = function () {
      $scope.isClickSearch = true;
      $scope.onSearchAbsence();
    };


  	/**
  	 * Export CSV
  	 * @return Void
  	 */
  	$scope.onExport = function () {

  	};
    /**
   * Reject Overtime Report by admin
   *
   * @return void
   */
  $scope.rejectRow = function (report) {
     $scope.isLoading = true;
    commonService.requestAPI('POST', ENV.host + '/reports/absence/' + report.id + '/reject', {},
        function(data){
          $scope.isLoading = false;
          if(data){
            $scope.total_report = (parseFloat($scope.total_report) - parseFloat(report.cost/60)).toFixed(2);
            $scope.reports = commonService.spliceRow(report, $scope.reports);
            $scope.total = $scope.reports.length + 1;
          }
      });
  };

  /**
   * Accept overtime report by admin
   * @param  report
   * @return void
   */
  $scope.acceptRow = function (report) {
     $scope.isLoading = true;
    commonService.requestAPI('POST', ENV.host + '/reports/absence/' + report.id + '/accept', {},
        function(data){
         $scope.isLoading = false;
         $scope.isAccept = true;
         $scope.fillItem(data);
         if(data){
          $scope.accept_total = (parseFloat($scope.accept_total) + parseFloat(report.cost/60)).toFixed(2);
          $scope.reports = commonService.spliceRow(report, $scope.reports);
          $scope.reports.unshift(data);
        }
      });
  };

    /**
     * set status Reject of button Reject
     * @param  report.id
     * @return Void
     */
     $scope.isReject = function (report){
      $scope.idReject = report.id;
      $scope.isRejectConfirm = true;
    };

    /**
     * Cancel Reject button
     * @return Void
     */
    $scope.isCancel = function () {
       $scope.isRejectConfirm = false;
    };
  	/**
     * get previous page
     * @return previous page
     */
    $scope.prevPage = function() {
    if ($scope.currentPage > 1) {
      $scope.currentPage--;
      $scope.getListReports($scope.currentPage);
    }
    };

    /**
     *  disabled previous page if the index of current page = 1
     */
    $scope.prevPageDisabled = function() {
      return $scope.currentPage === 1 ? "disabled" : '';
    };

    /**
     * get next page
     * @return next page
     */
    $scope.nextPage = function() {
      if($scope.total == $scope.reportsPerPage ){
        $scope.currentPage++;
        $scope.getListReports($scope.currentPage);
      }
    };

    /**
     * disabled next page if the index of current page is last page
     */
    $scope.nextPageDisabled = function() {
      return $scope.total < $scope.reportsPerPage  ? "disabled" : '';
    };

  });

