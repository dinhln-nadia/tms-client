'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:SettingSettingDasboardCtrl
 * @description
 * # SettingSettingDasboardCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('SettingSettingDasboardCtrl', function ($rootScope, $scope, $filter, $stateParams, $localStorage, commonService, ENV, $http, $injector, $timeout) {
    $scope.name = '';
    $scope.$storage = $localStorage;
    $scope.listDayOff = [];
    // Injector
    var $validationProvider = $injector.get('$validation');

    // Validator form login using angular-validation
    $scope.regForm = {
      checkValid: $validationProvider.checkValid,
      submit: function (form) {
        $validationProvider.validate(form);

        $scope.onLogin();
      }
    };

    /**
     * get init value for view
     * @return Void
     */
    $scope.initSetting_dasboard = function ()
    {
      if($localStorage.premission != 1 )
      {
        $state.go('app.report_missing_user');
      }
      $rootScope.pageHeader = $filter('translate')('menus.settings');
      $rootScope.pageDesc   = $filter('translate')('project.desc');
      $rootScope.menuActive = 'settings';
      $scope.getListDayOff();
      $scope.MessageNoi = $localStorage;
      $scope.MessageNoitify = $scope.MessageNoi.messageNoitify;
    };

    /**
     * get list categories and processes and save localStorage
     * @param  category or process type 
     * @return Void
     */
    $scope.getListCategories = function (categoryName)
    {
       $http({
        method: 'GET',
        url: ENV.host + '/categories?type=' + categoryName,
      })
      .success(function ( data, response) {
        switch(categoryName){
              case 'category':
               $localStorage.categories = data.data;
                break;

              case 'process':
               $localStorage.processes = data.data;
                break;
            }
        $scope.name = '';
        $scope.type = '';
      })

      .error(function ( data, status) {
        commonService.timeoutToken(status);
      });
    };

    /**
     * Create new Category or Process
     * @return Void
     */
    $scope.createRow = function ()
    {
      if(!$scope.name || !$scope.type)
      {
        alert('Missing data');
        return 0;
      }
      $http({
        method: 'POST',
        url: ENV.host + '/categories/',
        data: {
          name: $scope.name,
          type: $scope.type
        }
      })
      .success(function ( data, response) {
       if($scope.type == 'category'){
            $scope.getListCategories('category');
          } else {
             $scope.getListCategories('process');
          }
        alert('Create success!');
      })
      .error(function ( data, status) {
        commonService.timeoutToken(status);
      });
    }

    /**
     * Delete Category or Process
     * @param id of category or process
     * @return Void
     */
    $scope.onDeleteCategory = function (id, type)
    {

        $http({
          method: 'DELETE',
          url: ENV.host + '/categories/' + id,
        })
        .success(function ( data, response) {
          if(type == 1){
            $scope.getListCategories('category');
          } else {
             $scope.getListCategories('process');
          }
          // alert('Delete success!');
        })
        .error(function ( data, status) {
          commonService.timeoutToken(status);
        });
    }

    /**
     * Edit name category
     */
    $scope.editSetting = function (id, name, type) {
      if(type == 1){
        $scope.isLoading1 = true;
      }
      else {
        $scope.isLoading2 = true;
      }

      commonService.requestAPI('PUT', ENV.host + '/categories/' + id, { name: name },
        function(data){
          if(type == 1){
            $scope.getListCategories('category');
            $scope.isLoading1 = false;
          } else {
            $scope.getListCategories('process');
            $scope.isLoading2 = false;
          }
          $scope.isMessages = true;
          $scope.messages = ['success'];
          $scope.timeOut();
      });
    }

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() { 
           $scope.isMessages = false;
           $scope.isLoading1 = false;
           $scope.isLoading2 = false;
           $scope.messages = [];
        },2000);
  };

  /** 
   * insert dayoff by admin created
   * @param input
   */
   $scope.addDayOf = function () {
    if( !$scope.note || !$scope.dateDayoff) {
      return false;
    }
    $scope.isLoading3 = true;
    var day = new Date(Date.UTC($scope.dateDayoff.getFullYear(),$scope.dateDayoff.getMonth(),$scope.dateDayoff.getDate()));
    var date = day.toISOString();
    var time = $scope.formatTime(date);

    $http({
        method: 'POST',
        url: ENV.host + '/settings/install-dayoff/',
        data: { 
           day: time,
           note: $scope.note
        }
      })
      .success(function ( data, response) {
        $scope.getListDayOff();
        $scope.SuccessMessages = true;
        $scope.SuccessSms = 'Done !';
        $scope.timeOut();
        $scope.isLoading3 = false;
        $scope.note = '';
        $scope.dateDayoff = '';
      })

      .error(function ( data, status) {
        $scope.sms = data.error;
        $scope.timeOut();
        $scope.isLoading3 = false;
        commonService.timeoutToken(status);
      });
   }
   /**
   * get format time
   * @param  date
   * @return time
   */
  $scope.formatTime = function (date){
    var time;
    var year = date.substr(0,4);
    var month = date.substr(5,2);
    var date = date.substr(8,2);

    time = year + '-' + month + '-'+ date +' '+'00:00:00';
    return time;
  };

   /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() { 
         $scope.SuccessMessages = false;
         $scope.sms = '' ;
         $scope.SuccessSms = '';
         $scope.SuccessSmss = '';
         $scope.errorMess = '';
        },3000);

  };

  /**
   * Get List day off
   */
   $scope.getListDayOff = function () {
    $http({
        method: 'GET',
        url: ENV.host + '/settings/list-dayoff',
      })
      .success(function ( data, response) {
        $scope.listDayOff = data.getData;
      })

      .error(function ( data, status) {
        
      });
   }

   $scope.DeleteDay = function (day) {
    $scope.isLoading4 = true;
    $http({
        method: 'POST',
        url: ENV.host + '/settings/delete-dayoff',
        data: {
          day: day
        }
      })
      .success(function ( data, response) {
        $scope.isLoading4 = false;
        $scope.getListDayOff();
      })

      .error(function ( data, status) {
        
      });
   }


   $scope.addMessage = function (status) {
      if (!$scope.MessageNoitify & status == 1) {
        return false;
      }
      if($scope.MessageNoitify.length > 150 ) {
        $scope.errorMess = 'The 130 character limit number!';
        $scope.timeOut();
        return false;
      }
      $scope.errorMess = '';
      if(status == 0) {
        $scope.data = {'mes':''};
        $scope.MessageNoitify = '';
      }

      if(status == 1) {
        $scope.data = {'mes':$scope.MessageNoitify};
      }

      commonService.requestAPI('POST', ENV.host + '/settings/update-message', $scope.data,
      function(data) {
        $scope.SuccessSmss = 'success';
        $scope.MessageNoi.messageNoitify = data['message'];
        $scope.timeOut();
      });


    }

});
