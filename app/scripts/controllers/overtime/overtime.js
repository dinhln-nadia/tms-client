'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:OvertimeOvertimeCtrl
 * @description
 * # OvertimeOvertimeCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('OvertimeOvertimeCtrl', function ($scope, $window, $rootScope, $filter,$timeout, $stateParams, $localStorage, $http,commonService, ENV) {

  $scope.timeso =
  [
  '00:00','00:15','00:30','00:45','01:00','01:15','01:30','01:45','02:00','02:15','02:30','02:45','03:00','03:15','03:30',
  '03:45','04:00','04:15','04:30','04:45','05:00','05:15','05:30','05:45','06:00','06:15','06:30','06:45','07:00','07:15',
  '07:30','07:45','08:00','08:15','08:30','08:45','09:00','09:15','09:30','09:45','10:00','10:15','10:30','10:45','11:00',
  '11:15','11:30','11:45','12:00','12:15','12:30','12:45','13:00','13:15','13:30','13:45','14:00','14:15','14:30','14:45',
  '15:00','15:15','15:30','15:45','16:00','16:15','16:30','16:45','17:00','17:15','17:30','17:45','18:00','18:15','18:30',
  '18:45','19:00','19:15','19:30','19:45','20:00','20:15','20:30','20:45','21:00','21:15','21:30','21:45','22:00','22:15',
  '22:30','22:45','23:00','23:15','23:30','23:45','24:00'
  ];
  $scope.types = [ { id:1 , name: 'Overtime Pending'},{id:3 , name: 'Overtime Accepted'}, {id:4 , name: 'Overtime Rejected'}];
  $scope.reports  = [];
  $scope.messages = [];
  $scope.option = 1;
  $scope.checkBug = true;
  $scope.search_project = '' ;
  $scope.isClickSearch = true;
  $scope.checkM = true;
  $scope.isNew = false;
  $scope.initOverTime = function () {
    $scope.category_list = $localStorage.categories;
    $scope.project_list = $localStorage.projects;
    $scope.processes_list = $localStorage.processes;
    $scope.isMessages = false;
    $scope.isLoading = false;
    $scope.getListReports();
    $rootScope.menuActive = 'overtime';
    if($scope.data_search) {

    }
  }
  // remove user
  $scope.removeUser = function(index) {
    $scope.isNew = false;
    $scope.reports.splice(index, 1);
  };

  // add user
  $scope.addRow = function() {
    $scope.inserted = {
      id: 'add_new'
    };
    $scope.isNew = true;
    $scope.reports.unshift($scope.inserted);
  };

   /**
   * Get data reports
   *
   * @return void
   */
  $scope.getListReports = function () {
    var url;
    $scope.isLoading = true;
      var start_time;
      var end_time;
      var day = new Date();
      $scope.year = day.getFullYear();
      $scope.month = day.getMonth()+1;
      start_time = $scope.getStartTime();
      end_time = $scope.getEndTime();
      if( $scope.data_search) {

        var checkMonth = $scope.data_search.substr(49,2);

        if($scope.month != parseInt(checkMonth) ) {
          $scope.checkM = false;
        } else {
          $scope.checkM = true;
        }
        url = ENV.host + '/reports/search?' + $scope.data_search + 'option='+ $scope.option;
        $scope.data_search = '';

      } else {

        url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&option='+ $scope.option;

      }
      commonService.requestAPI('GET', url, {},
      function(data){
         $scope.isLoading = false;
        $scope.isMessages = false;
        if(data){
          $scope.total = data.length;
          var total = 0;
          var accept_total = 0;
          var weekend = 0;
          data.forEach(function(entry) {
              total = total + parseInt(entry.cost);
              if(entry['type'] == 3){
                accept_total = accept_total + parseInt(entry.cost);
               var day_name = new Date(entry.start_time).getDay();
               if(day_name == 0 || day_name ==6){
                  weekend = weekend + parseInt(entry.cost)
               }
              }
          });
          $scope.reports = $scope.fillTemplate (data);
          $scope.total_report = (total/60).toFixed(2);
          $scope.accept_total = (accept_total/60).toFixed(2);
          $scope.weekend = (weekend/60).toFixed(2);
        }
      });
  };
  /**
   * get format time
   * @param  date
   * @return time
   */
  $scope.formatTime = function (date, time, status){
    var start_time;
    var year = date.substr(0,4);
    var month = date.substr(5,2);
    var date = date.substr(8,2);

    // if (status == 'add_new') {
    //   date = parseInt(date) +1;
    // }

    start_time = year + '-' + month + '-'+ date +' '+time+':00.000';
    return start_time;
  };

 /**
   * get Start time
   * @param  date
   * @return start_time
   */
  $scope.getStartTime = function (day){
    var start_time;
    if(!day){
        start_time = new Date();
        start_time = start_time.toISOString();
        var year = start_time.substr(0,4);
        var month = start_time.substr(5,2);
        var day = '01';
    } else  {
        start_time = day;
        var year = start_time.substr(0,4);
        var month = start_time.substr(5,2);
        var day = start_time.substr(8,2);
    }


    start_time = year + '-' + month + '-'+ day +' '+'00:00:01.000';
    return start_time;
  };
  /**
   * get End time
   * @param  date
   * @return  end_time
   */
  $scope.getEndTime = function (date) {
    var end_time;
    if(!date){
        end_time = new Date();
    } else  {
      end_time = date;
    }
    end_time = end_time.toISOString();
    var year = end_time.substr(0,4);
    var month = end_time.substr(5,2);
    var date = end_time.substr(8,2);
    date = parseInt(date) +1 ;
    end_time = year + '-' + month + '-'+ date + ' ' +'00:00:01.000';

    return end_time;

  };

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() {
         $scope.isMessages = false;
         $scope.SuccessMessages = false;
         $scope.messages = [];
        },5000);

  };

  $scope.deleteMessage = function () {
    $scope.isMessages = false;
    $scope.SuccessMessages = false;
  }
  /*
   * Save User
   */
  $scope.saveUser = function (data, id_row,report) {

    // $scope.checkBug = false;
    var isTwentyfour = $scope.validateReport(data);
    if($scope.messages.length == 0){

      if(id_row == 'add_new') {
        var day = new Date(Date.UTC(data['date'].getFullYear(),data['date'].getMonth(),data['date'].getDate()));
        var date = day.toISOString();
        data.start_time = $scope.formatTime(date, data.start_time, 'add_new');
        data.end_time = $scope.formatTime(date, data.end_time, 'add_new');
        data['type'] = 1;
        $scope.CreateReport(data, report);
      } else {
        var day = new Date(data['date']);
        var date = day.toISOString();
        data.start_time = $scope.formatTime(date, data.start_time, 'edit');
        data.end_time = $scope.formatTime(date, data.end_time, 'edit');
        data['type'] = 1;
        $scope.UpdateReport(data,report,id_row);
      }
    }
      else {
        $scope.isMessages = true;
        $scope.SuccessMessages = false;
        $scope.timeOut();
        return 'error';
     }
     if($scope.checkBug == true) {
        return 'error';
      }
  }
  /**
     * Validate input
     * @param  data
     * @return value check time is 24:00
     */
  $scope.validateReport = function (data) {
    var nullStart = commonService.checkNull(data.start_time, 'Start time');
    if(nullStart !=0){
      $scope.messages.push(nullStart);
    }
    var isStartTime = commonService.checkTime(data.start_time, 'Start time');
    if(isStartTime !=0){
      $scope.messages.push(isStartTime);
    }
    var nullEnd = commonService.checkNull(data.end_time , 'End time');
    if(nullEnd !=0){
      $scope.messages.push(nullEnd);
    }
    if(data.end_time){
      var isTwentyfour = commonService.checkEndDay(data.end_time);
    }
    if (!isTwentyfour){
     var isEndTime = commonService.checkTime(data.end_time, 'End time');
        if(isEndTime!=0){
          $scope.messages.push(isEndTime);
        }
    }
    if($scope.messages.length == 0){
    $scope.checkGreater(data.start_time, data.end_time);
    }
    if(data.ticket){
      var isTicket = commonService.checkTicket(data.ticket);
    if(isTicket !=0){
      $scope.messages.push(isTicket);
      }
    }
    var nullProject = commonService.checkNull(data.project_id, 'Project');
    if(nullProject !=0){
      $scope.messages.push(nullProject);
    }
    var nullProcess = commonService.checkNull(data.process_id,'Process');
    if(nullProcess != 0){
      $scope.messages.push(nullProcess);
    }
    var nullCategory = commonService.checkNull(data.category_id , 'Category');
    if(nullCategory !=0){
       $scope.messages.push(nullCategory);
    }
    var nullNote = commonService.checkNull(data.note , 'Note');
    if(nullNote !=0){
    $scope.messages.push(nullNote);
    }
    return isTwentyfour;
  };
  /**
  * check end_time > start_time
  * @param start_time field
  * @param  end_time field
  * @return true/false
  */
  $scope.checkGreater = function (start_time, end_time) {
    start_time = $scope.getFormatTime(start_time,$scope.isOldDay);
    end_time = $scope.getFormatTime(end_time,$scope.isOldDay);
    if(start_time >= end_time){
      $scope.messages.push('End time must be greater than Start time');
  }
  };
  /**
   * change format date (UTC)
   * @param  date
   * @return date
   */
  $scope.getFormatTime = function(time, statusOld) {
    var m;
    var d;
    var y;
    var month;
    month = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    if(statusOld || $scope.isCheckedMissing){
      m = parseInt($scope.oldDay.substr(5,2)) -1;
      y = $scope.oldDay.substr(0,4);
      d = $scope.oldDay.substr(8,2);
    } else {
      var today = new Date();
       m = today.getMonth();
       d = today.getDate();
       y = today.getFullYear();
       d = commonService.insertStringZero(d);
     }
      time = time.toString();
      var h = time.substr(0,2);
      var i = time.substr(3,2);
      var s = '00';
      var date = y+'-'+month[m]+'-'+d+' '+h+':'+i+':00.000';

      return date;

  };

  /**
  * Create new report
  * @param data and report index
  * @param Void
  */
  $scope.CreateReport = function (data, report) {
    $scope.isLoading = true;
    var cost = report.cost;
      $http({
        method:'POST',
        url: ENV.host + '/reports',
        data:data,
      }).success(function (response, status){
        $scope.isLoading = false;
        $scope.messages.push(response.status);
        if($scope.messages){
          $scope.SuccessMessages = true;
          $scope.timeOut();
        }
        var data = response.data;
        $scope.fillItem(data);
        if(report.$$hashKey) {
          $scope.reports = commonService.spliceRow(report, $scope.reports);
        }
        $scope.reports.unshift(data);
        $scope.limit_row ++;
        if(data){
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60)).toFixed(2);
        }
        $scope.checkBug = false;
      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.fillItem(data);
        if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
          $scope.checkBug = true;
        }
        commonService.timeoutToken(status);
        $scope.timeOut();
      });
  };

  /**
   * change start_time and end_time to 00:00 format
   * @param  data (start_time and end_time ICT format)
   * @return data
   */
  $scope.fillItem = function (data) {
    data['date'] = data.start_time.substr(0,10);
    data['start_time'] = data.start_time.substr(11,5);
    data['end_time'] = data.end_time.substr(11,5);
    return data;
    };

      /**
   * Change start_time and end_time from date to time
   * @param  data from server
   * @return data after change
   */
  $scope.fillTemplate = function (data) {
    data.forEach (function(item){
     $scope.fillItem(item);
    });

    return data;
  };

  /**
     * set name as id of category/process/project
     * @param   id
     * @param   list
     * @return name
     */
  $scope.findName = function(id, data) {
    if(data){
      for (var i = 0; i < data.length; i++) {
      if(data[i].id == id){

        return data[i].name;
      }
    }
  }
  return  null;

  };
 /**
  * Update Report
  * @param data and report index
  * @param Void
  */
  $scope.UpdateReport = function (data, report, id) {
    $scope.isLoading = true;
     var cost = report.cost;
    $http({
        method: 'PUT',
        url: ENV.host + '/reports/' + id
        ,
        data: data,
      }).success(function (response, status){
        $scope.messages.push(response.status);
        $scope.isLoading = false;
        if($scope.messages){
          $scope.SuccessMessages = true;
          $scope.timeOut();
        }
        var data = response.data;
        $scope.fillItem(data);
        if(data){
        $scope.reports = commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
        $scope.total_report = (parseFloat($scope.total_report) +  parseFloat(data.cost/60) - parseFloat(cost/60)).toFixed(2);
        }

      }).error(function (response, status) {
        $scope.isLoading = false;
        $scope.colorShowMessages = true;
        $scope.fillItem(data);
         if(response){
          $scope.messages.push(response.error);
          $scope.isMessages = true;
        }

        $scope.isNew = false;
        commonService.timeoutToken(status);
      });
    };

    $scope.onSearchReport = function (start, end, data) {
      if(data){
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      $scope.search_project = data['search_project'];
      $scope.search_member = data['search_member'];
      $scope.search_type = data['search_type'];
      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      $scope.data_search = '';
      if($scope.search_project){
        $scope.data_search = 'project_id=' + $scope.search_project + '&';
      }
      if ($scope.search_member){
        $scope.data_search  += 'user_id='+ $scope.search_member + '&';
      }
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.search_type != "undefined" && $scope.search_type != null && $scope.search_type !== ''){
         $scope.data_search += 'type=' + $scope.search_type + '&';
      }

      $scope.isSearch = true ;
      if($scope.isClickSearch){
        $scope.currentPage = 1;
        $scope.getListReports($scope.currentPage);
      }

    };


     /**
   * Delete row in local
   *
   * @return void
   */
  $scope.deleteRow = function (report) {
     $scope.isLoading = true;
    var cost = report.cost;
    commonService.requestAPI('DELETE', ENV.host + '/reports/' + report.id, {},
        function(data){
           $scope.isLoading = false;
           $scope.colorShowMessages = false;
        $scope.reports = commonService.spliceRow(report, $scope.reports);
         $scope.total_report = (parseFloat($scope.total_report) - parseFloat(cost/60)).toFixed(2);
      });
  };

    /**
   * edit button
   * @param  row form
   * @return void
   */
  $scope.isEdit = function (rowform) {
    rowform.$show();
  };

  $scope.onClear = function () {
    $scope.data_search = '';
  }

    /**
   * cancel button when edit
   * @param  rowform
   * @return Void
   */
  $scope.isCancel = function (rowform) {
    rowform.$cancel();
    $scope.isAdd = true;
    if($scope.messages){
        $scope.timeOut();
     }
  };
 /**
   * get out Row
   * @param  report id
   * @return Void
   */
  $scope.getOutRow = function (report) {
    if($scope.messages){
      $scope.timeOut();
    }
   $scope.reports = commonService.spliceRow(report, $scope.reports);
    $scope.limit_row ++;
    $scope.isAdd = true;
    if($scope.limit_row ==2 ) {
      $scope.isNew = false;
      $scope.isAdd = true;
    }
  };

});
