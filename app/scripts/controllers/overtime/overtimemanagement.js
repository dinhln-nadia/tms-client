'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:OvertimeOvertimemanagementCtrl
 * @description
 * # OvertimeOvertimemanagementCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('OvertimeOvertimemanagementCtrl', function ($scope, $window, $rootScope, $filter,$timeout, $stateParams, $localStorage, $http,commonService, ENV) {
  $scope.GetHost = ENV.host;
 	 $scope.types = [{ id:1 , name: 'Overtime Pending'},{id:3 , name: 'Overtime Accepted'}, {id:4 , name: 'Overtime Rejected'}];
 	//option  = 1 for overtime management
  $scope.option = 1;
 	$scope.search_project = '' ;
  $scope.search_member = '' ;
  //show detail time for weekend
  $scope.detailWeekend = false;
    //pagination
	$scope.currentPage = 1;
	$scope.reportsPerPage = 20;
	$scope.reports = [];
 	/**
 	 * init value for ovetime management
 	 * @return void
 	 */
   $scope.initOvertimeManagement = function () {
   	$rootScope.pageHeader = $filter('translate')('report.title');
    $rootScope.pageDesc   = $filter('translate')('menus.overtime');
    $rootScope.menuActive = 'overtime_management';
    $scope.getListReports($scope.currentPage);
    $scope.$storage = $localStorage;
    
    
   };
   
   /**
   * Get data reports
   *
   * @return void
   */
  $scope.getListReports = function (page_id) {
     $scope.isPagination = true;
    var url;
    $scope.isLoading = true;
      var start_time;
      var end_time;
      var day = new Date();
      $scope.year = day.getFullYear();
      $scope.month = day.getMonth()+1;
      start_time = $scope.getStartTime();
      end_time = $scope.getEndTime();
      if($scope.data_search){ 
        $scope.isPagination = false;
        if($localStorage.premission !=1) {
          url = ENV.host + '/reports/search?option='+ $scope.option + '&user_id=' + $localStorage.user.id +'&'+ $scope.data_search;
        } 
      	url = ENV.host + '/reports/search?option='+ $scope.option +'&'+ $scope.data_search ; 
      } else  {
        if($localStorage.premission !=1){
          $scope.isPagination = false;
        url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&option='+ $scope.option + '&user_id=' + $localStorage.user.id;
        } 
        url = ENV.host + '/reports/search?start_time=' + start_time + '&end_time=' + end_time + '&option='+ $scope.option + '&count=' + $scope.reportsPerPage + '&page=' + page_id; 
      }
    commonService.requestAPI('GET', url, {},
      function(data){
         $scope.isLoading = false;
        $scope.isMessages = false;
        if(data){
          $scope.total = data.length;
          var total = 0;
          var accept_total = 0; 
          var weekend = 0;
          data.forEach(function(entry) {
              total = total + parseInt(entry.cost);
              if(entry['type'] == 3){
                accept_total = accept_total + parseInt(entry.cost);
               var day_name = new Date(entry.start_time).getDay();
               if(day_name == 0 || day_name ==6){
                  weekend = weekend + parseInt(entry.cost)
               }
              }
          });
          $scope.reports = $scope.fillTemplate (data);
          $scope.total_report = (total/60).toFixed(2);
          $scope.accept_total = (accept_total/60).toFixed(2);
          $scope.weekend = (weekend/60).toFixed(2);
        }
      });

  };
  /**
   * Delete row in local
   *
   * @return void
   */
  $scope.deleteRow = function (report) {
     $scope.isLoading = true;
    var cost = report.cost;
    commonService.requestAPI('DELETE', ENV.host + '/reports/' + report.id, {},
        function(data){
          $scope.isLoading = false;
          $scope.reports =commonService.spliceRow(report, $scope.reports);
          $scope.total_report = (parseFloat($scope.total_report) - parseFloat(cost/60)).toFixed(2);
      });
  };

  

  /**
   * edit button 
   * @param  row form 
   * @return void
   */ 
  $scope.isEdit = function (report) {
    $scope.isDeleteConfirm = true;
    $scope.idDelete = report.id;
  };

  /**
   * cancel button when edit
   * @param  rowform 
   * @return Void
   */
  $scope.isCancel = function () {
     $scope.isDeleteConfirm = false;
     $scope.isRejectConfirm = false;
  };

  /**
   * Change start_time and end_time from date to time
   * @param  data from server
   * @return data after change
   */
  $scope.fillTemplate = function (data) {
    data.forEach (function(item){
     $scope.fillItem(item);
    });

    return data;
  };

 
  /**
   * get Start time
   * @param  date
   * @return start_time
   */
  $scope.getStartTime = function (){
    var start_time;
    var start;
   	start = commonService.insertStringZero($scope.month);
    start_time = $scope.year + '-' + start + '-'+ '01'+' '+'00:00:00.000';
    return start_time;
  };


  /**
   * get End time
   * @param  date
   * @return  end_time
   */
  $scope.getEndTime = function () {
  	var end;
    var end_time;
  	end = commonService.insertStringZero($scope.month + 1);
    end_time = $scope.year + '-' + end + '-'+ '01'+' '+'00:00:00.000';
    return end_time;

  };

 /**
    * change minutes to hours
    * @param  cost
    * @return Void
    */
  $scope.getCost = function (cost) {
     return (cost/60).toFixed(2);
  };
  
  /**
   * change start_time and end_time to 00:00 format
   * @param  data (start_time and end_time ICT format)
   * @return data
   */
  $scope.fillItem = function (data) {
  	data['date'] = data.start_time.substr(0,10);
    data['start_time'] = data.start_time.substr(11,5);
    data['end_time'] = data.end_time.substr(11,5);
    return data;
    };

  $scope.isReject = function (report) {
    $scope.isRejectConfirm = true;
    $scope.idReject = report.id;
  };

  /**
   * click close notification
   * @return Void
   */
  $scope.deleteMessage = function () {
    $scope.messages = [];
    $scope.isMessages = false;
  };

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() { 
           $scope.isMessages = false;
           $scope.messages = [];
        },1000);

  };

  /**
   * Reject Overtime Report by admin
   *
   * @return void
   */
  $scope.rejectRow = function (report) {
     $scope.isLoading = true;
    commonService.requestAPI('POST', ENV.host + '/reports/overtime/' + report.id + '/reject', {},
        function(data){
        $scope.isLoading = false;
        $scope.messages = [];
        $scope.total_report = (parseFloat($scope.total_report) - parseFloat(report.cost/60)).toFixed(2);
        $scope.reports =commonService.spliceRow(report, $scope.reports); 
        $scope.total =  $scope.reports.length +1 ;
        
      });
  };

  /**
   * Accept overtime report by admin
   * @param  report
   * @return void
   */
  $scope.acceptRow = function (report) {
  	 $scope.isLoading = true;
    commonService.requestAPI('POST', ENV.host + '/reports/overtime/' + report.id + '/accept', {},
        function(data){
         $scope.isLoading = false;
         $scope.isAccept = true;
         $scope.fillItem(data);
         if(data){
        $scope.accept_total = (parseFloat($scope.accept_total) + parseFloat(report.cost/60)).toFixed(2);
        var date = new Date(report.date).getDay();
        if(date == 0 || date ==6){
          $scope.weekend = (parseFloat($scope.weekend) + parseFloat(report.cost/60)).toFixed(2);
        }
        $scope.reports = commonService.spliceRow(report, $scope.reports);
        $scope.reports.unshift(data);
        }
      });
  };

  /**
     * set name as id of category/process/project/user
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data) {
      if(data){
        for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){
          return data[i].name;
        }
      }
    }

      return null;
      
    };

    $scope.isDetail = function () {
      $scope.detailWeekend = true;

    };
    $scope.isCancelDetail = function () {
      $scope.detailWeekend = false;
    };

    /**
     * get time date
     * @param  start_time
     * @return date time and check over 5/nex month and different user
     */
    $scope.getTime = function (time) {
      var y = parseInt(time.substr(0,4));
      var m = parseInt(time.substr(5,2));
      if(((y == $scope.year) &&( m == $scope.month))||((y == $scope.year) && (m == $scope.month -1) && ($scope.date <=5))) {
        $scope.isOverMonth = false;
      } else {
         $scope.isOverMonth = true;
      }

      return time ;
    };

  
    /**
     * search on project_id and user_id
     * @return Void
     */
    $scope.onClickSearch = function () {
      $scope.isClickSearch = true;
      $scope.onSearchReport();
    }; 

    $scope.onSearchReport = function (start, end, data) {
      if(data){
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      $scope.search_project = data['search_project'];
      $scope.search_member = data['search_member'];
      $scope.search_type = data['search_type'];
      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      $scope.data_search = '';
      if($scope.search_project){
        $scope.data_search = 'project_id=' + $scope.search_project + '&';
      }
      if ($scope.search_member){
        $scope.data_search  += 'user_id='+ $scope.search_member + '&';
      }
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.search_type != "undefined" && $scope.search_type != null && $scope.search_type !== ''){
         $scope.data_search += 'type=' + $scope.search_type + '&';
      }
      $scope.isSearch = true ;
      if($scope.isClickSearch){
        $scope.currentPage = 1;
        $scope.getListReports($scope.currentPage);
      }

    };

    /**
     * clear search fields
     * @return Void
     */
    $scope.onClear = function () {
      $scope.search_member = '';
      $scope.search_project = '' ;
      $scope.start_time = '';
      $scope.end_time = '';
      $scope.search_type = '';
      $scope.data_search = '';
      $scope.hasStartEnd = false;
      $scope.getListReports($scope.currentPage);
    };

    /**
     * get previous page
     * @return previous page
     */
    $scope.prevPage = function() {
    if ($scope.currentPage > 1) {
      $scope.currentPage--;
      $scope.getListReports($scope.currentPage);
    }
    };

    /**
     *  disabled previous page if the index of current page = 1
     */
    $scope.prevPageDisabled = function() {

      return $scope.currentPage === 1 ? "disabled" : '';
    };

    /**
     * get next page
     * @return next page
     */
    $scope.nextPage = function() {
      if($scope.total == $scope.reportsPerPage){
        $scope.currentPage++;
        $scope.getListReports($scope.currentPage);
      }
    };

    /**
     * disabled next page if the index of current page is last page
     */
    $scope.nextPageDisabled = function() {

      return $scope.total < $scope.reportsPerPage ? "disabled" : '';
    };

    /**
     * Export excel
     */
     $scope.Export = function (id) {
      $window.open(ENV.download + '/export-excel/' + id+'&'+'tocken'+'='+$localStorage.token);
        // commonService.requestAPI('GET', ENV.host + '/export-excel/' + id+'&'+'tocken'+'='+$localStorage.token, {},function(data){

        // });
     }

});
