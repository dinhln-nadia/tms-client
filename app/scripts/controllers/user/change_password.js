'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:UserChangePasswordCtrl
 * @description
 * # UserChangePasswordCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .directive('pwCheck', [function () {    
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    }
  }])
  .controller('UserChangePasswordCtrl', function ($scope, $location, $http, $filter, $state, tmsDefines, ENV, $localStorage) {

    $scope.old_password = '';
    $scope.password = '';
    /**
     * Intialize function for changePassWord controller
     * @return void
     */
    $scope.initChangePassword = function () {
        $scope.user_id = $localStorage.user.id;
    };

		/**
     * Process change Password click
     *
     * @return void
     */
    $scope.onChange = function () {
      // check form has data ?
      if (! $scope.old_password || ! $scope.password || !$scope.password_confirmation) {
        return false;
      }

      // Send change Password request
      $http({
        method: 'POST',
        url: ENV.host + '/users/change-password',
        data: {
          email:$localStorage.user.email,
          old_password: $scope.old_password,
          password: $scope.password,
          password_confirmation: $scope.new_password

        }
      }).success(function (data, status) {
       	var message = $filter('translate')('change_password.messages.success_change');
        // Show success message
      	window.alert(message);
        $state.go('app.profile_user',{id: $localStorage.user.id});
      }).error(function (data, status) {
        commonService.timeoutToken(status);
      	window.alert(data.error);
      });
    };
    
  });

