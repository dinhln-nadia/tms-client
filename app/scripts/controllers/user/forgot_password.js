'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:UserForgotPasswordCtrl
 * @description
 * # UserForgotPasswordCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('UserForgotPasswordCtrl', function ($scope, UserService, $http, ENV, $translate, $filter, $state) {

    /**
     * Intialize function for forgot password controller
     *
     * @return void
     */
    $scope.initForgotPassword = function(){
      // Get questions from API
      // UserService.getQuestions($scope.processQuestions);

      // Set default question combobox
      // $scope.question_id = '1';
    };

    // Enter send email
    $scope.send_email = function () 
    {
      $scope.onForgot();
    }

    /**
     * Process submit click for forgot password form
     *
     * @return void
     */
    $scope.onForgot = function(){
      $scope.isLoading = true;

      if(! $scope.email){
        // Submit fail
        window.alert($filter('translate')('common.miss_input'));

        return false;
      }

      $http({
        method: 'POST',
        url: ENV.host + '/users/sendEmail',
        data: {
          email: $scope.email,
        }
      })
      .success(function (data, status){
         $scope.isLoading = false;
        // Post success
        // Show new password information for user
        window.alert($filter('translate')('register.new_password'));
        $state.go('login');
      })
      .error(function (data, status){
        $scope.isLoading = false;
        // Post fail
        commonService.timeoutToken(status);

        // Show error infomation
        window.alert(data.error);

        return false;
      });
    };
  });
