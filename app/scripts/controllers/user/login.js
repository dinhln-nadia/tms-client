'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:UserLoginCtrl
 * @description
 * # UserLoginCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.onLogin();
                event.preventDefault();
            }
        });
    };
})
  .controller('UserLoginCtrl', function ($scope, $http, $localStorage, $filter, $injector, $state, $location, tmsDefines, ENV) {

    $scope.email = '';
    $scope.password = '';
    $scope.test='fffff';
    // Injector
    var $validationProvider = $injector.get('$validation');

    // Validator form login using angular-validation
    $scope.loginForm = {
      checkValid: $validationProvider.checkValid,
      submit: function (form) {
        $validationProvider.validate(form);

        $scope.onLogin();
      }
    };

    /**
     * Intialize function for login controller
     *
     * @return void
     */
    $scope.initLogin = function () {
      // Auto login when exist token and user data.
      if ($localStorage.token && $localStorage.user) {
        // Login dashboard if login success.
        $state.go('app.report_main');

        return false;
      }

      // Initialize for icheck library.
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    };

    /**
     * Process login click
     *
     * @return void
     */
    $scope.onLogin = function () {
      // check form has data ?    
      if (! $scope.email || ! $scope.password) {
        return false;
      }

      // Send login request
      $scope.isLoading = true;
      $http({
        method: 'POST',
        url: ENV.host + '/users/login',
        data: {
          email: $scope.email,
          password: $scope.password
        }
      }).success(function (data, status) {
        $scope.isLoading = false;
        if (data.token) {
          // Save to local token.
          $localStorage.token = data.token;
          $localStorage.user = data.user;

          $localStorage.premission = data.premission;
          $localStorage.CheckMissingReport = false;
          //redirect
          if($localStorage.premission != 1 )
          {
            //Login dashboard if login success.
            $state.go('app.report_main');
          }
          else
          {
             // Login dashboard if login success.
            $state.go('app.report_main');
          }

        }
      }).error(function (data, status) {
        $scope.isLoading = false;
        var message = $filter('translate')('login.messages.error_login');
        // Show fail message
      	window.alert(message);
      });
    };

  });
