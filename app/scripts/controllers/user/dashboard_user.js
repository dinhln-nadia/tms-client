'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:UserDashboardUserCtrl
 * @description
 * # UserDashboardUserCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .directive('pwCheck', [function () {    
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.pwCheck;
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('pwmatch', v);
          });
        });
      }
    }
    }])
  .controller('UserDashboardUserCtrl', function ($scope, $http, $filter, UserService, ENV, $localStorage, $state, $stateParams, $rootScope, tmsDefines) {
    $scope.email = '';
    $scope.password = '';
    $scope.name = '';
    $scope.answer = '';
    $scope.userDetail = [];
    // pagination
    $scope.userPerPage = 10;
    $scope.currentPage = 1;

    // if($scope.password_confirmation  !=  $scope.password)
    // {
    //   document.getElementById("#password_confirmation_error").innerHTML = "Password do not match!";
    // }

    $scope.initDataUser = function ()
    {
      if($localStorage.premission != 1 )
      {
        $state.go('app.project_list');
      }

      $rootScope.pageHeader = $filter('translate')('menus.users');
      $rootScope.pageDesc   = $filter('translate')('users.list');
      $rootScope.menuActive = 'users';
      $scope.listUser($scope.currentPage);
    }
    // init register user
    $scope.initRegister =function ()
    {
        UserService.getQuestions($scope.processQuestions);
    }

    //init edit infor user
    $scope.initEditUser = function ()
    {
        if($localStorage.premission != 1 )
        {
          $state.go('app.project_list');
        }
        $rootScope.pageHeader = $filter('translate')('menus.users');
        $rootScope.pageDesc   = $filter('translate')('users.list');
        $rootScope.menuActive = 'users';
        UserService.getQuestions($scope.processQuestions);
        $scope.userGetDetail($stateParams.id);

    }
    
    $scope.onclickEditUser = function (id) {
       $state.go('app.users_edit',{ id : id});
    };

    $scope.redirectDashboard = function ()
    {
      $state.go('app.user');
    }

    $scope.initProfile = function ()
    {
      UserService.getQuestions($scope.processQuestions);
      $scope.userGetDetail($stateParams.id);
    }
    /**
    * Change pass user
    */
    $scope.changepass = function () {
      $state.go('app.change_password');
    }

    $scope.saveRegister = function ()
    {
        if(! $scope.name || ! $scope.email || ! $scope.password || ! $scope.password_confirmation){
        // Submit fail
        alert($filter('translate')('common.miss_input'));

        return false;
        }
        // request to get question data 
        $http({
          method: 'POST',
          url: ENV.host + '/users',
          data: {
            name: $scope.name,
            email: $scope.email,
            password: $scope.password,
            password_confirmation: $scope.password_confirmation
          }
        })
        .success(function(data, status){
          // Post success
          if (data.token) {
            $localStorage.token = data.token;
          }

          // Show success message
          window.alert($filter('translate')('register.register_success'));
          $state.go('app.user');
        })
        .error(function(data, status){
          // Post fail
          window.alert(data.error.email);
          commonService.timeoutToken(status);
          return false;
        });
    };

    /**
     * Process data question. Put data into question combobox
     *
     * @return void
     */
    $scope.processQuestions = function (data){
      // Add data to questions model
      $scope.questions = data;
    };

    $scope.listUser = function(page_id) 
    {
      $http({
          method: 'GET',
          url: ENV.host + '/users?page='+ page_id,
        })
        .success(function ( data, response) {
          $scope.userDetail = data.data;
        })
        .error(function ( data, status) {
          commonService.timeoutToken(status);
        });
    }

    // get detail user

    $scope.userGetDetail = function ( $user_id ) 
    {
      $http({
          method: 'GET',
          url: ENV.host + '/users/' + $user_id,
        })
        .success(function (data, response) {
          $scope.userDetail = data.data;
        })
        .error(function (data, status) {
          commonService.timeoutToken(status);
        });
    }

    $scope.addNewUser = function ()
    {
        $state.go('app.users_register');
    }

    /**
    *Process edit user
    *
    */
    $scope.updateDetailUser = function ($type)
    {
      if(! $scope.userDetail.name || ! $scope.userDetail.email){
        // Submit fail
        alert('Missing data!'); 

        return false;
      }

      $http({
          method: 'PUT',
          url: ENV.host + '/users/' + $stateParams.id,
          data: {
            name: $scope.userDetail.name,
            email: $scope.userDetail.email
          }
        })
        .success(function(data, status){
          // Post success
          if (data.token) {
            $localStorage.token = data.token;
          }
          alert('Edit success!');
          if( $type == 'admin' ){

            $state.go('app.user');

          }
        })
        .error(function (data, status) {
            window.alert(data.error.email);
            commonService.timeoutToken(status);
            // alert('Edit error');
        });
    }

    //Delete user
    $scope.onDeleteUser = function (id)
    { 
      $scope.isLoading = true;

      if($localStorage.premission != 1 )
      {
        $state.go('app.project_list');
      }
      
      $http({
        method: 'DELETE',
        url: ENV.host + '/users/' + id,
      })
      .success(function (data, status) {
        $scope.isLoading = false;
        for (var i = $scope.userDetail.length - 1; i >= 0; i--) {
          if($scope.userDetail[i].id == id)
          {
            $scope.userDetail.splice(i, 1);
             break;
          }
        };
      })
      .error(function (data, status) {
        $scope.isLoading = false;
        commonService.timeoutToken(status);
      });
    }

    $scope.unlockUserLink = function ()
    {
      $state.go('app.User_block');
    }

    $scope.backUser = function ()
    {
      $state.go('app.user', {page: 1});
    }

    //
    // Show list user has lock
    //

    $scope.initUserBlock = function ()
    {
      $http({
        method: 'GET',
        url: ENV.host + '/users/list-disable/',
      })
      .success(function (data, status) {
        $scope.userBlock = data.data;
      })
      .error(function (data, status) {
        commonService.timeoutToken(status);
      });
    }

    //
    // Unlock user
    //

    $scope.unLockUser = function (id)
    {
      $scope.isLoading = true;
      $http({
        method: 'POST',
        url: ENV.host + '/users/enable',
        data: {
          users: id + ','
        }
      })
      .success(function (data, status) {
        $scope.isLoading = false;
        $scope.userBlock = $scope.initUserBlock();
      })
      .error(function (data, status) {
        $scope.isLoading = false;
        commonService.timeoutToken(status);
      });
    }

    /*
     * Edit password user ( Admin )
     */

     $scope.onclickEditPassUser = function (id)
     {

       $state.go('app.admin_change_pass',{id: id});
     
     }

     $scope.initAdminChangePassword = function ()
     {

        $scope.id = $stateParams.id;


     }

     $scope.onChangePass = function (id)
     {
        if($localStorage.premission != 1 )
        {

          $state.go('app.project_list');
          
        }
      
        if($scope.password == '')
        {

          alert('Missing data.');
          return false;

        }

        // Send change Password request
        $http({
          method: 'POST',
          url: ENV.host + '/users/change-password-admin',
          data: {
            password: $scope.password,
            id: id 
          }
        }).success(function (data, status) {
          var message = $filter('translate')('change_password.messages.success_change');
          // Show success message
          window.alert(message);
          $state.go('app.user');
        }).error(function (data, status) {
          commonService.timeoutToken(status);
          window.alert(data.error);
        });

     }
    /**
     * get previous page
     * @return previous page
     */
    $scope.prevPage = function() {
    if ($scope.currentPage > 1) {
      $scope.currentPage--;
      $scope.listUser($scope.currentPage);
    }
    };

    /**
     *  disabled previous page if the index of current page = 1
     */
    $scope.prevPageDisabled = function() {
      return $scope.currentPage === 1 ? "disabled" : '';
    };

    /**
     * get next page
     * @return next page
     */
    $scope.nextPage = function() {
      if($scope.userDetail.length == $scope.userPerPage){
        $scope.currentPage++;
        $scope.listUser($scope.currentPage);
      }
    };

    /**
     * disabled next page if the index of current page is last page
     */
    $scope.nextPageDisabled = function() {
      return $scope.userDetail.length < $scope.userPerPage ? "disabled" : '';
    };

});