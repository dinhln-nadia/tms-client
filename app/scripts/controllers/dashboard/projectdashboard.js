'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:DashboardProjectdashboardCtrl
 * @description
 * # DashboardProjectdashboardCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('ProjectdashboardCtrl', function ($rootScope, $scope, $filter, $stateParams, $localStorage, commonService, ENV) {
      $scope.projects = [];
      $scope.data_search = '';
      $scope.start_time = '';
      $scope.end_time = '';

      $scope.page_id = 1;
      $scope.maxPage =0;
      $scope.pre = true;
      $scope.next = true;
      $scope.projectId = $stateParams.project_id;
    /**
     * Initialize function for dashboard controller.
     *
     * @return void
     */
    $scope.initProjectDashboard = function () {
      $rootScope.pageHeader = $filter('translate')('dashboard.title');
      $rootScope.pageDesc   = $filter('translate')('dashboard.project');
      $rootScope.menuActive = 'dashboard_project';
      $scope.getListProjects();
      $scope.$storage = $localStorage;
      $scope.getDataProjectDashboard();
      $scope.costUserKeyCategory    = {};
      $scope.category               = {};
    };

    /**
     * get List project
     * @return Void
     */
    $scope.getListProjects = function() {
    commonService.requestAPI('GET', ENV.host + '/projects', {},
      function(data) {
        $scope.projects = data;
      });
    };

    /**
     * set name as id of project
     * @param   id
     * @param   list
     * @return name
     */
    $scope.findName = function(id, data) {
      for (var i = 0; i < data.length; i++) {
        if(data[i].id == id){
          return data[i].name;
        }
      }
    };

    $scope.onSearchDashboard = function (start, end, data) {
      $scope.data_search = '';
      if(data){
        start = start.substr(0,10) + ' '+ '23:59:00.000';
        end = end.substr(0,10)+ ' '+'23:59:00.000';
        $scope.start_time =  start;
        $scope.end_time = end;
        $scope.search_project = data['search_project'];
      }
      if($scope.search_project){
        $scope.data_search = 'project_id=' + $scope.search_project + '&';
        $scope.projectId =$scope.search_project;
      } else {
        $scope.data_search = 'project_id=' + $stateParams.project_id + '&';
      }
      if( $scope.start_time && $scope.end_time ){
        $scope.hasStartEnd = true;
      }
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      if($scope.isClickSearch){
        $scope.getDataProjectDashboard();
      }
    };

    $scope.onClickSearch = function () {
      $scope.isClickSearch = true;
      $scope.onSearchDashboard ();
    };

    /**
     * clear search fields
     * @return Void
     */
    $scope.onClear = function () {
      $scope.start_time = '' ;
      $scope.end_time = '';
      $scope.search_project = '';
      $scope.data_search = '';
      $scope.hasStartEnd = false;
      $scope.getListDashboard ();
    };

     // Get data for my dashboard
  $scope.getDataProjectDashboard = function ()
  {
    var url; 
    if(!$scope.data_search){
        url =  ENV.host + '/projects/project-dashboard?project_id=' + $stateParams.project_id ;   
    } else {
         url =  ENV.host + '/projects/project-dashboard?' + $scope.data_search ;
    }
    commonService.requestAPI('GET',url, {},
      function(data){
        if(data['user'].length <= 0) {
            $scope.data                      = [];
            $scope.costUserKeyCategory       = [];
            $scope.categoryProject           = [];
            $scope.members                   = [];
            $scope.processProject            = [];
            $scope.isMessages = true;
          } else {
            $scope.data                      = data['data'];
            $scope.costUserKeyCategory       = data['cost_user'];
            $scope.categoryProject           = data['category'];
            $scope.members                   = data['user'];
            $scope.processProject            = data['process'];
            $scope.colors = ["#00aec8", "#8D8D8D", "#3c8dbc", "#f56954", "#056b3b" ,"#008B8B","#D3D3D3","#FF69B4","#778899","#6A5ACD","#008080","#A0522D"];
            $scope.isMessages = false;
          }

          if( ($scope.members.length) > 5 ) {
            $scope.next = false;
          }
          else {
            $scope.next = true; 
          }
          // get max page
          if( ($scope.members.length % 5 ) != 0) {
            $scope.maxPage = parseInt(($scope.members.length /5) + 1);
          } else {
            $scope.maxPage = $scope.members.length /5;
          }

          $scope.getDataDefault($scope.page_id);
          
      });
  };
    // push item in array paginate
    $scope.getDataDefault = function (page_id) {
      $scope.dataMember = [];
      for(var i = (page_id * 5)-5; i< (page_id * 5) ; i++) {

        if(angular.isUndefined($scope.members[i]) == false) {
          $scope.dataMember.push($scope.members[i]);
        }

      }
   
    };

    // button nextpage
    $scope.nextPage = function () {
      $scope.page_id = $scope.page_id + 1;
      $scope.getDataDefault($scope.page_id);
      $scope.pre = false;
      $scope.next = ($scope.page_id == $scope.maxPage);
    };

    // button preview page
    $scope.prePage = function () {
      $scope.page_id = $scope.page_id - 1;
      $scope.getDataDefault($scope.page_id);

      $scope.next = false;
      $scope.pre = ($scope.page_id == 1);
    };



});

