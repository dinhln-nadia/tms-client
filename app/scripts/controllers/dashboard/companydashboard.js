'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:DashboardCompanydashboardCtrl
 * @description
 * # DashboardCompanydashboardCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('CompanydashboardCtrl', function ($rootScope, $scope, $filter, $stateParams, $localStorage, commonService, ENV, $state) {
    $scope.data_search = '';
    /**
     * Initialize function for dashboard controller.
     *
     * @return void
     */

    $scope.initCompanyDashboard = function () {
      $scope.isShowDashboard = true;
      $rootScope.pageHeader = $filter('translate')('dashboard.title');
      $rootScope.pageDesc   = $filter('translate')('dashboard.company_dashboard');
      $rootScope.menuActive = 'dashboard_company';

      $scope.data = {};
      $scope.xLabel = {};
      $scope.getDataCompanyDashboard();
      $localStorage.data_dashboard = [];
      $scope.page_id = 1;
      $scope.maxPage =0;
      $scope.isMessages = false;
      $scope.pre = true;
      $scope.next = true;
      
    };

    $scope.onSearchDashboard = function (start, end) {
      $scope.data_search = '';
      start = start.substr(0,10) + ' '+ '23:59:00.000';
      end = end.substr(0,10)+ ' '+'23:59:00.000';
      $scope.start_time =  start;
      $scope.end_time = end;
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time ;
      }
      $scope.getDataCompanyDashboard();
    };
    
    // Get data for my dashboard
    $scope.getDataCompanyDashboard = function ()
    { 
      var url;
      if(!$scope.data_search){
        url = ENV.host + '/projects/company-dashboard';
      } else {
        url = ENV.host + '/projects/company-dashboard?' + $scope.data_search;
      }
      commonService.requestAPI('GET',url, {},
        function(data){
          $scope.data_search = '';
          if(data[0].length <= 0) {
            $scope.data = data[0];
            $scope.xLabel = data[1];
            $scope.colors = ["#00aec8", "#8D8D8D", "#3c8dbc", "#f56954", "#056b3b" ,"#008B8B","#D3D3D3","#FF69B4","#778899","#6A5ACD","#008080","#A0522D"];
            $scope.isMessages = true;
            $scope.dataCost = [];
          } else {
            $scope.data = data[0];
            $scope.xLabel = data[1];
            $scope.colors = ["#00aec8", "#8D8D8D", "#3c8dbc", "#f56954", "#056b3b" ,"#008B8B","#D3D3D3","#FF69B4","#778899","#6A5ACD","#008080","#A0522D"];
            $scope.isMessages = false;

            if( ($scope.data.length) >5 ) {
              $scope.next = false;
            }
            else {
              $scope.next = true; 
            }
            // get max page
            if( ($scope.data.length % 5 ) != 0) {
              $scope.maxPage = parseInt(($scope.data.length /5) + 1);
            } else {
              $scope.maxPage = $scope.data.length /5;
            }

            $scope.getDataDefault($scope.page_id);
            }
        });
    }

    // push item in array paginate
    $scope.getDataDefault = function (page_id) {
      $scope.dataCost = [];
      for(var i = (page_id * 5)-5; i< (page_id * 5) ; i++) {

        if(angular.isUndefined($scope.data[i]) == false) {
          $scope.dataCost.push($scope.data[i]);
        }

      }      
    }

    // button nextpage
    $scope.nextPage = function () {
      $scope.page_id = $scope.page_id + 1;
      $scope.getDataDefault($scope.page_id);
      $scope.pre = false;
      $scope.next = ($scope.page_id == $scope.maxPage);
    }

    // button preview page
    $scope.prePage = function () {
      $scope.page_id = $scope.page_id - 1;
      $scope.getDataDefault($scope.page_id);

      $scope.next = false;
      $scope.pre = ($scope.page_id == 1);
    }


    $scope.linkProjectDashboard = function (project_id)
    {
      $state.go('app.project_dashboard', {project_id: project_id});
    }
});

