'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:DashboardMydashboardCtrl
 * @description
 * # DashboardMydashboardCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('MydashboardCtrl', function ($rootScope, $scope, $filter, $stateParams, $localStorage, commonService, ENV) {
    $scope.start_time = '';
    $scope.end_time = '';
    $scope.data_search = '';


    /**
     * Initialize function for dashboard controller.
     *
     * @return void
     */
    $scope.initMyDashboard = function () {
      $rootScope.pageHeader = $filter('translate')('dashboard.title');
      $rootScope.pageDesc   = $filter('translate')('dashboard.my_dashboard');
      $rootScope.menuActive = 'dashboard';
      $scope.getDataMyDashboard();
      $scope.isMessages = false;

      $scope.pageHours = 1;
      $scope.pageTicket = 1;

      $scope.maxPage = 0;

      $scope.preHours = true;
      $scope.preTicket = true;

      $scope.nextHours = true;
      $scope.nextTicket = true;

    };


    $scope.onSearchDashboard = function (start, end) {
      $scope.data_search = '';
      start = start.substr(0,10) + ' '+ '23:59:00';
      end = end.substr(0,10)+ ' '+'23:59:00';
      $scope.start_time =  start;
      $scope.end_time = end;
      if ($scope.start_time) {
        $scope.data_search += 'start_time=' + $scope.start_time + '&';
      }
      if ($scope.end_time) {
        $scope.data_search += 'end_time=' + $scope.end_time + '&';
      }
      $scope.getDataMyDashboard();

    };


  // Get data for my dashboard
  $scope.getDataMyDashboard = function ()
  { 
    var url ; 
    if(!$scope.data_search){
      url = ENV.host + '/projects/my-dashboard'; 
    } else {
      url = ENV.host + '/projects/my-dashboard?' + $scope.data_search;
    }
    commonService.requestAPI('GET', url, {},
      function(data){
        $scope.data_search = '';
         if(data[0].length <= 0) {
            $scope.data = [];
            $scope.xLabel = {};
            $scope.isMessages = true;
          } else {
            $scope.data = data[0];
            $scope.xLabel = data[1];
            $scope.colors = ["#00aec8", "#8D8D8D", "#3c8dbc", "#f56954", "#056b3b" ,"#008B8B","#D3D3D3","#FF69B4","#778899","#6A5ACD","#008080","#A0522D"];
            $scope.isMessages = false;
          }
          // get max page
          $scope.maxPage = $scope.getMaxPage($scope.data);
          // set default button next
          if( ($scope.data.length) >4 ) {
            $scope.nextHours = false;
            $scope.nextTicket = false;
          }
          else {
            $scope.nextHours = true;
            $scope.nextTicket = true; 
          }

          $scope.getDataDefault($scope.pageHours, 1);
          $scope.getDataDefault($scope.pageTicket, 2);
      });
  }

    $scope.getMaxPage = function (dataPage) {
      // get max page
      if( (dataPage.length % 4 ) != 0) {
        return parseInt((dataPage.length /4) + 1);
      } else {
         return dataPage.length /4;
      }
    }

      // push item in array paginate
    $scope.getDataDefault = function (page_id, chart) {
      if(chart == 1) {
        $scope.dataHours = []; // chart == 1
        for(var i = (page_id * 4)-4; i< (page_id * 4) ; i++) {
          if(angular.isUndefined($scope.data[i]) == false) {
            $scope.dataHours.push($scope.data[i]);
          }
        }
      } 
      if(chart == 2) {
        $scope.dataTicket = []; // chart == 2
        for(var i = (page_id * 4)-4; i< (page_id * 4) ; i++) {
          if(angular.isUndefined($scope.data[i]) == false) {
            $scope.dataTicket.push($scope.data[i]);
          }
        }
      } 
    }

    // button nextpage
    $scope.nextPageHours = function () {
      $scope.pageHours = $scope.pageHours + 1;
      $scope.getDataDefault($scope.pageHours, 1);
      $scope.preHours = false;
      $scope.nextHours = ($scope.pageHours == $scope.maxPage);
    }

    // button preview page
    $scope.prePageHours = function () {
      $scope.pageHours = $scope.pageHours - 1;
      $scope.getDataDefault($scope.pageHours, 1);

      $scope.nextHours = false;
      $scope.preHours = ($scope.pageHours == 1);
    }

    // button nextpage
    // ticket char
    $scope.nextPageTicket = function () {
      $scope.pageTicket = $scope.pageTicket + 1;
      $scope.getDataDefault($scope.pageTicket, 2);
      $scope.preTicket = false;
      $scope.nextTicket = ($scope.pageTicket == $scope.maxPage);
    }

    // button preview page
    $scope.prePageTicket = function () {
      $scope.pageTicket = $scope.pageTicket - 1;
      $scope.getDataDefault($scope.pageTicket, 2);

      $scope.nextTicket = false;
      $scope.preTicket = ($scope.pageTicket == 1);
    }
});
