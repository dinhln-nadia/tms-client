'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:AppCtrl
 * @description
 * # AppCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
  .controller('AppCtrl', function ($rootScope, $scope, $localStorage, $state, $filter, tmsDefines,commonService, ENV, $http,$timeout) {

    var toDay = new Date();
    $scope.month = toDay.getMonth() + 1;

    if($localStorage.premission == 1 )
    {
      $scope.menus = [
        {slug: 'project', name: $filter('translate')('menus.project'), link: 'app.project_list'},
        {slug: 'dashboard', name: $filter('translate')('dashboard.my_dashboard'), link: 'app.my_dashboard', menu_items: []},
        {slug: 'dashboard_company', name: $filter('translate')('dashboard.company_dashboard'), link: 'app.company_dashboard'},
        {slug: 'reports', name: $filter('translate')('menus.reports'), link: 'app.report_main'},
        {slug: 'missing_reports', name: $filter('translate')('menus.missing_reports'), link: 'app.report_missing'},
        {slug: 'users', name: $filter('translate')('menus.users'), link: 'app.user'},
        {slug: 'settings', name: $filter('translate')('menus.settings'), link: 'app.setting_dashboard'},
        {slug: 'overtime_management', name: $filter('translate')('menus.overtime'), link: 'app.overtime_management'},
        {slug: 'vacation', name: $filter('translate')('menus.vacation_report'), link: 'app.vacation_user', menu_items: [
          {slug: 'vacation_management', name: $filter('translate')('menus.vacation'), link: 'app.vacation_management'},
        ]},
      ];
    }  
    else
    {
      $scope.menus = [
        {slug: 'my_dashboard', name: $filter('translate')('dashboard.my_dashboard'), link: 'app.my_dashboard'},
        {slug: 'reports', name: $filter('translate')('menus.reports'), link: 'app.report_main'},
        {slug: 'missing_reports', name: $filter('translate')('menus.missing_reports'), link: 'app.report_missing_user({month:month })'},
        {slug: 'overtime', name: $filter('translate')('menus.overtime_report'), link: 'app.over-time', menu_items: [
          {slug: 'overtime_management', name: $filter('translate')('menus.overtime'), link: 'app.overtime_management'},
          ]},
        {slug: 'vacation', name: $filter('translate')('menus.vacation_report'), link: 'app.vacation_user'},
      ];
    }
    /**
     * Function for append version code for ng-include template.
     *
     * @param path
     * @returns {*}
     */
    $scope.getInclude = function (path) {
      if (ENV.version) {
        return path + '?v=' + ENV.version;
      }

      return path;
    };
     
    /**
     * Initialize function for header layout.
     *
     * @return void
     */
    $scope.initNavbar = function () {

      // Active push menu.
      $.AdminLTE.pushMenu.activate('[data-toggle="offcanvas"]');

      // Active control sidebar.
      //$.AdminLTE.controlSidebar.activate();

      //Enable sidebar tree view controls
      $.AdminLTE.tree('.sidebar');

      $scope.user = $localStorage.user;
      $localStorage.reportsperpages = [{id:20, name: '20'},{id:40, name: '40'},{id:60, name: '60'},{id:80, name: '80'},{id:100, name: '100'},{id:'All', name: 'All'}];
      $scope.getListMissing();
      $scope.getListCategories('category');
      $scope.getListCategories('process');
      $scope.getListProjects();
      $scope.getAllProjects();
      $scope.getListUsers();
      $scope.getAllUsers();
      $localStorage.dataMissing = [];
      $scope.localMissing = $localStorage;
      $scope.getMessageTop();
    };

    /**
     * get list missing report 
     * @return Void
     */
    $scope.getListMissing = function ()
    { 

      $scope.beforeDeadline = false;
          $scope.year = toDay.getFullYear();
          $scope.date = toDay.getDate();
          var url1 = 'month='+ $scope.month + '&year='+ $scope.year;
          if($scope.date <=5){
            $scope.beforeDeadline = true;
            var url2= 'month='+ ($scope.month -1)+ '&year='+ $scope.year;
          } 
        commonService.requestAPI('GET', ENV.host + '/users/missing-report?'+url1 , {},
        function(data){
            $localStorage.dataMissing = data;
        });
        if($scope.beforeDeadline){
        commonService.requestAPI('GET', ENV.host + '/users/missing-report?' + url2, {},
        function(data){
            $localStorage.dataMissing1 = data;

        });
        }
    };

    /**
   * Get data categories
   *
   * @return void
   */
  $scope.getListCategories = function (categoryName) {
    commonService.requestAPI('GET', ENV.host + '/categories?type=' + categoryName, {},
      function(data) {
       
        // request success
        switch(categoryName){
          case 'category':
             $localStorage.categories = data;
            break;
          case 'process':
           $localStorage.processes = data;
            break;
        }
      });
  };
   /**
   * Get data projects
   *
   * @return void
   */
  $scope.getListProjects = function() {
    commonService.requestAPI('GET', ENV.host + '/projects', {},
      function(data) {        
        $localStorage.projects = data; 
      });

  };

  /**
   * get all projects (disable and enable)
   * @return {[type]} [description]
   */
  $scope.getAllProjects = function () {
     commonService.requestAPI('GET', ENV.host + '/projects/id-name', {},
      function(data) {
        $localStorage.allProjects = data; 

      });
  };
   /**
     * get List user
     * @return Void
     */
    $scope.getListUsers = function () {
      commonService.requestAPI('GET', ENV.host + '/users', {},
      function(data) {
        $localStorage.users = data;
      });
    };

    /**
     * get all users (disable and enable)
     * @return void
     */
    $scope.getAllUsers = function () {
     commonService.requestAPI('GET', ENV.host + '/users/id-name', {},
      function(data) {
        $localStorage.allUsers = data;
      });

    };


    /**
     * Logout user
     *
     * @return void
     */
    $scope.onLogout = function () {
      // Clear token
      delete $localStorage.token;
      delete $localStorage.categories ;
      delete $localStorage.processes ;
    // Get list projects
      delete $localStorage.projects ;
      delete $localStorage.allProjects ;
      delete $localStorage.user ;
      delete $localStorage.users;
      delete $localStorage.allUsers;
      delete $localStorage.premission;

      // Goto logout
      $state.go('login');
    };

    /**
     * Initialize function for app controller.
     *
     * @return void
     */
    $scope.initApp = function () {
      // Redirect to login if not exist token.
      if (! $localStorage.token) {
        $state.go('login');
      }
    };

    /**
    * redirect to profile user.
    */
    $scope.profileUser = function (id)
    {
      $state.go('app.profile_user',{ id : id});

    };

  /**
   * Hide notification
   * @return Void
   */
    $scope.timeOut = function () {
      $timeout(function() { 
        commonService.requestAPI('GET', ENV.host + '/settings/getMessage', {},
          function(data) {
          $localStorage.messageNoitify = data;
        });
        $scope.getMessageTop();
      },10000);
    };

    $scope.getMessageTop = function () {
      $scope.timeOut();
    }

  });
