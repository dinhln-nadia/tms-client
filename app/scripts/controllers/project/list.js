'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ProjectListCtrl
 * @description
 * # ProjectListCtrl
 * Controller of the TMSApp
 */
angular.module('TMSApp')
    // confirm delete directive
  .controller('ProjectListCtrl', function ($scope, $http, $rootScope, $localStorage, commonService, $stateParams, $filter, $state, ENV ) {
    $scope.search_project = '';
    // pagination
    $scope.projectsPerPage = 10;
    $scope.currentPage = 1;
    //select disable(1) and enable(0)
    $scope.option = 0;
    $scope.tail = '';
    $scope.projects = '';
    $scope.isDisable = ($stateParams.disable == 'list-disable') ? true : false;

    // press enter search
    $scope.searchProject = function ()
    {
      $scope.onSearchProject();
    };
    /**
    * Intialize function for first data
    * @return Void
    */
    $scope.initListProjects = function () {
      if($localStorage.premission != 1 )
      {
        $state.go('app.report_missing_user');
      }
      $rootScope.pageHeader = $filter('translate')('project.enable');
      $rootScope.pageDesc   = $filter('translate')('project.desc');
      $rootScope.menuActive = 'project';

      $scope.getListTail();

      // Get project data for first load.
      $scope.getProjectsData();

    };

    /**
     * Intialize function for get Data
     * @return list projects
     */
    $scope.getProjectsData = function () {
      var url = '';
        if (! $scope.search_project) {
          url = ENV.host + '/projects' + $scope.tail ;
        } else {
          url = ENV.host + '/projects/search?name=' + $scope.search_project  + '&option=' + $scope.option;
        }
      $http({
        method: 'GET',
        url: url

      }).success(function (data, status){
        $scope.projects = data.data;
        $scope.isLoading = false;
      }).error(function (data, status) {
        commonService.timeoutToken(status);
      });
    };

    
    /**
     * change state for get list disable projects
     * @return Void
     */
    $scope.getDisableState = function () {
      if($scope.isDisable){
      $scope.option = 1;
     }
    };

    /**
     * set tail for url 
     * @return text url
     */
    $scope.getListTail= function () {
      if($scope.isDisable){
        $scope.tail = '/list-disable';
        $scope.option = 1;
        $rootScope.pageHeader = $filter('translate')('project.disable');
        $rootScope.pageDesc   = $filter('translate')('project.desc');
      } else {
         $scope.tail = '';
      }
    };


    /**
     * Intialize function for detail button
     * @param  int id
     * @return Void
     */

    $scope.onDetail = function (id) {
       $state.go('app.project_detail',{ id : id});
    };

    /**
     * Intialize function for delete button
     * @param  int id
     * @return Void
     */
    $scope.onChange = function (project) {
      $scope.isLoadingProject = true;
      var method;
      var url;
      if($scope.isDisable) {
       method = 'POST',
       url = ENV.host + '/projects/'+project.id +'/toggle_status'
      } else {
        method = 'DELETE',
        url = ENV.host + '/projects/'+project.id
      }
      $http({

        method: method,
        url: url
      }).success(function (data, status){
        $scope.isLoadingProject = false;
        $scope.projects = commonService.spliceRow(project,$scope.projects);
      }).error(function (data, status) {
        $scope.isLoadingProject = false;
        commonService.timeoutToken(status);
      });
    };

    /**
     * Intialize function for search button
     * @return list project
     */
    $scope.onSearchProject = function () {
        $scope.isLoading = true;
        $scope.getProjectsData($scope.currentPage);
    };

    /**
     * get previous page
     * @return previous page
     */
    // $scope.prevPage = function() {
    // if ($scope.currentPage > 1) {
    //   $scope.currentPage--;
    //   $scope.getProjectsData($scope.currentPage);
    // }
    // };

    /**
     *  disabled previous page if the index of current page = 1
     */
    // $scope.prevPageDisabled = function() {
    //   return $scope.currentPage === 1 ? "disabled" : '';
    // };

    /**
     * get next page
     * @return next page
     */
    // $scope.nextPage = function() {
    //   if($scope.projects.length == $scope.projectsPerPage){
    //     $scope.currentPage++;
    //     $scope.getProjectsData($scope.currentPage);
    //   }
    // };

    /**
     * disabled next page if the index of current page is last page
     */
    // $scope.nextPageDisabled = function() {
    //   return $scope.projects.length < $scope.projectsPerPage ? "disabled" : '';
    // };

  });
