'use strict';

/**
 * @ngdoc function
 * @name TMSApp.controller:ProjectDetailCtrl
 * @description
 * # ProjectDetailCtrl
 * Controller of the TMSApp
 */
var app = angular.module('TMSApp');

app.
  controller('ProjectDetailCtrl', function ($rootScope, $scope, ENV, $timeout, $http, $localStorage, $stateParams, $window, commonService, $filter, $state) {
  $scope.userIds = [];
  $window.checkedUids = [];
  $scope.messages = [];
  $scope.isMessages = false;
  // $scope.userPerPage = 10;
  // $scope.currentPage = 1;
  $scope.listUser = [];

  /**
   * Initialize function for detail project controller.
   *
   * @return void
   */
  $scope.initDetailProject = function () {
    if($localStorage.premission != 1 )
    {
      $state.go('app.report_missing_user');
    }
    $rootScope.menuActive = 'project';
    $rootScope.isLoadModal = false;
    // Check is edit or add
    if($stateParams.id === '' || $stateParams.id === 'add'){
      $scope.project = { id: '', name: '' };              // init project
      $scope.allowAddUser = false;  // don't allow add users to project
      $scope.isEdit = false;        // can't edit
    } else {
      $scope.project = { id: $stateParams.id, name: '' }; // init project
      $scope.getData();             // try get user data from server
      // $scope.isEdit = true;         // can edit
    }

    // Add New Project - list user selected to add to project
    $scope.listUserAdded = '';
    if($stateParams.id && !commonService.checkPositiveFloat($stateParams.id, 'id of projet') ) {
      $scope.getFreeTime();
    }
    
  }

  $scope.onCheckedUserId = function (user_id) {
    if (!user_id) {
      return false;
    }
    $scope.userIds.push(user_id);
  };

  /**
   * Get Data user in project
   *
   * @return boolean
   */
  $scope.getData = function () {
    commonService.requestAPI('GET', ENV.host + '/projects/' + $scope.project.id, '', function(data){
        if(data.length != 0){
          // Get user information
          $scope.users = data[0].users;
          $scope.isEdit = true;  

          // Get project information
          $scope.project = {
            id: data[0].id,
            name: data[0].name,
            quotation_time: data[0].quotation_time,
            plan_time: data[0].plan_time,
          };
        }else{

          window.alert($filter('translate')('project.edit.not_data'));
          // show add new project form
          $scope.isEdit = false;
          $scope.project = { id: '', name: '' };
        }
    },
    function(data){
        window.alert('error');
    }

    );
  };

  /**
   * Function to delete user from project.
   *
   * @param id
   * @return void
   */
  $scope.deleteUser = function (user) {
        $scope.isLoadingDeleteUser = true;
        commonService.requestAPI('POST', ENV.host + '/projects/' + $stateParams.id + '/remove-user', {users_id: user.id.toString()},
          function(data){
            $scope.isLoadingDeleteUser = false;
            $scope.isMessages = true;
            $scope.colorShowMessages = false;
            $scope.messages.push($filter('translate')('project.remove.success '));
            $scope.timeOut();
            $scope.getFreeTime();
            $scope.users =commonService.spliceRow(user, $scope.users);
          });
  };

  /**
   * click close notification
   * @return Void
   */
  $scope.deleteMessage = function () {
    $scope.messages = [];
    $scope.isMessages = false;
  };

  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOut = function () {
    $timeout(function() { 
           $scope.isMessages = false;
        },1000);

  };
  /**
   * Hide notification
   * @return Void
   */
  $scope.timeOutBootom = function () {
    $timeout(function() { 
           $scope.isMessages_bootom = false;
        },2000);

  };
  /**
   * Search by filter.
   *
   * @return boolean
   */
  $scope.search = function (row) {

    // Search in data
    return (angular.lowercase(row.name).indexOf($scope.searchInputEdit || '') !== -1 ||
            angular.lowercase(row.email).indexOf($scope.searchInputEdit || '') !== -1 ||
            (row.id).toString().indexOf($scope.searchInputEdit || '') !== -1);
  };

  /**
   * Function to add new project.
   *
   *
   * @return void
   */
  $scope.addProject = function () {
    $scope.messages = [];
    $scope.validateProject();
    if($scope.messages.length != 0){
      $scope.isMessages = true;
      $scope.colorShowMessages = true;
    }else{
      $scope.isLoading = true;
      $scope.isMessages = false;
        $http({
        method: 'POST',
        url: ENV.host + '/projects',
        data: {
          name:  $scope.project.name, 
          quotation_time: $scope.project.quotation_time,
          plan_time: $scope.project.plan_time,
        } 
      }).success(function (data, status){
         // show form add list user
          $scope.isLoading = false;
          $scope.allowAddUser = true;
          $scope.project.id = data.data.id;
          $scope.isMessages = true;
          $scope.colorShowMessages = false;
          $scope.messages.push($filter('translate')('project.add.success'));
          $timeout(function() { 
          $scope.isMessages = false;
          $scope.messages = [];
          $state.go('app.project_detail',{ id : $scope.project.id });
          },2000);
      }).error(function (data, status){
        $scope.colorShowMessages = true;
         $scope.messages.push('Error Creat New Project');
          commonService.timeoutToken(status);   
      });  
    } 
  };

   $scope.validateProject = function () {

    var nullName = commonService.checkNull( $scope.project.name ,'Project Name');
    if(nullName !=0){
      $scope.messages.push(nullName);
    } 
    if($scope.project.quotation_time){
      var isFloatQuotation = commonService.checkPositiveFloat($scope.project.quotation_time , 'Quotation' );
      if(isFloatQuotation !=0){
      $scope.messages.push(isFloatQuotation);
      }
      var checkMax = commonService.checkMax($scope.project.quotation_time , 'Quotation');
      if(checkMax != 0){
        $scope.messages.push(checkMax);
      }
    }
    if($scope.project.plan_time){
      var isFloatPlan = commonService.checkPositiveFloat($scope.project.quotation_time, 'Plan');
    if(isFloatPlan !=0){
      $scope.messages.push(isFloatPlan);
      }
    }
    // if(isFloatQuotation == 0 && isFloatPlan ==0){ console.log(7);
      // if($scope.project.quotation_time < $scope.project.plan_time) { console.log(8);
      //  $scope.messages.push($filter('translate')('project.greater'));
      // }
    // }
   };



  /**
   * Function load iCheck library for checkbox
   *
   * @return void
   */
  $scope.loadICheck = function () {
    // Add iCheck library
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    // Add function to handle click checkbox
    $('.mailbox-messages ins').click(function(){
      // get checkbox
      var parent = $(this).parent();
      if(parent.has('input[class="user-check-ids select-user add-user"]').length != 0){
        // click checkbox in modal box

        return false;
      }
    });

    //Enable check and uncheck all functionality in modal add user
    if(!$rootScope.isLoadModal){

      $("#modal-add-user").click(function () {
        var clicks = $(this).data('clicks');
        if (clicks) {
          //Uncheck all checkboxes
          $(".user-check-ids").iCheck("uncheck");
          $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
          //Check all checkboxes
          $(".user-check-ids").iCheck("check");
          $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);

      });

      //Enable check and uncheck all functionality in add project form
      $("#add-user-checkall").click(function () {
        console.log('add project checkall ');

        var clicks = $(this).data('clicks');
        if (clicks) {
          //Uncheck all checkboxes
          $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
          $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
          $scope.listUserAdded = '';
        } else {
          //Check all checkboxes
          $(".mailbox-messages input[type='checkbox']").iCheck("check");
          $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);

      });

      // Loaded model
      $rootScope.isLoadModal = true; 
    }

    $('.list-user-added').on('ifChecked', function(event){
      $scope.selectUser($(this).data('id'));
    });

    $('.list-user-added').on('ifUnchecked', function(event){
      $scope.unselectUser($(this).data('id'));
    });

    // Assign checked user ids after change checkbos state.
    $('.user-check-ids').on('ifChecked', function (event) {
      var user_id = $(this).data('id');
      if (user_id) {
        if ($window.checkedUids.indexOf(user_id) < 0) {
          $window.checkedUids.push(user_id);
        }
      }
    });

    $('.user-check-ids').on('ifUnchecked', function (event) {
      var user_id = $(this).data('id');
      if (user_id) {
        if ($window.checkedUids.indexOf(user_id) >= 0) {
          $window.checkedUids.splice($window.checkedUids.indexOf(user_id), 1);
        }
      }
    });

  };

  /**
   * Function to add id user when select user checkbox.
   *
   * @return void
   */
  $scope.selectUser = function(id){
    // check list user id added project empty or not
    if($scope.listUserAdded != '')
    {
      $scope.listUserAdded += ',' + id;
    }
    else{
      $scope.listUserAdded = '' + id;
    }
  };

  /**
   * Function to add id user when select user checkbox.
   *
   * @return void
   */
  $scope.unselectUser = function(id){
    var arr = [];

    // check list user id added project empty or not
    if($scope.listUserAdded != ''){
      // convert string to array
      arr = $scope.listUserAdded.split(',');

      for (var i = 0; i < arr.length; i++) {
        if(arr[i] == id){
          arr.splice(i, 1); // remove element
        }
      };

      // convert array to string
      $scope.listUserAdded = arr.toString();
    }
  };

  /**
   * Function to update project.
   *
   * @return void
   */
  $scope.updateProjectName = function(id){
    $scope.messages = [];
    $scope.validateProject();
    if($scope.messages.length != 0){
      $scope.isMessages = true;
       $scope.colorShowMessages = true;
    }else{
      $scope.isLoading = true;
       $scope.isMessages = false ;
       $http({
        method: 'PUT',
        url:ENV.host + '/projects/' + id,
        data: {
          name:  $scope.project.name, 
          quotation_time: $scope.project.quotation_time,
          plan_time: $scope.project.plan_time,
          id: id
        } 
      }).success(function (data, status){
        $scope.isLoading = false;
        $scope.isMessages = true;
        $scope.colorShowMessages = false;
        $scope.messages.push($filter('translate')('project.update.success '));
        $scope.timeOut();
        $scope.getFreeTime();
      }).error(function (data, status) {
        $scope.colorShowMessages = true;
        $scope.messages.push('Error Update Project'); 
         commonService.timeoutToken(status);    
      });
    
    }
  };

  /**
   * get detail project
   * @param  project id
   * @return Void
   */
  $scope.project = function($project_id) {
        $http({
          method: 'GET',
          url: ENV.host + '/projects/' + $project_id,
        })
          .success(function (data, response) {
            $scope.project = data.data[0];
          })
          .error(function (data, status) {
            console.log(data);
            commonService.timeoutToken(status);  
          });
      };

  /**
   * get list user not in project
   * @param  project _id
   * @return Void
   */
  $scope.listUserNotInProject = function($project_id)
  {

    $http({
          method: 'GET',
          url: ENV.host + '/projects/' + $project_id + '/users_not_in' ,
        })
        .success(function (data, response) {
          $scope.listUser = data.data;
        })
        .error(function (data, status) {
          commonService.timeoutToken(status);  
        });

      $window.checkedUids = [];
  };

  /**
   * check before submit add user into project 
   * @return Void
   */
  $scope.onSubmitAssignUser = function () {
    $scope.isMessagesModal = false;
    if($window.checkedUids.length == 0)
    {
      $scope.isMessagesModal = true;
      $scope.colorShowMessages = true;
      $scope.messages.push($filter('translate')('project.add.please_choose'));
      return false;
    }
    $http({
          method: 'POST',
          url: ENV.host + '/projects/' + $scope.project.id + '/add-user',
          data: {
            users_id: $window.checkedUids.join(),
          }
        })
        .success(function (data, response) {
          // Close modal when success add user to project.
          $('#tms_project_add_user').modal('hide');

          $scope.getData();
          $scope.listUserAdded = '';
        })
        .error(function (data, status) {
           commonService.timeoutToken(status);  
          window.alert('Error : ' + data.error);
        });
    $window.checkedUids = [];
  };


  $scope.addQuotation = function (data, user)
  {
  
    if(user.member_quotation == '')
    {
      alert("Missing data.");
      return false;
    }
    if($scope.freeTime == null)
    {
      $scope.isMessages_bootom = true;
      $scope.colorShowMessages = true;
      $scope.message = 'Quotation time of project is undefined!';
      return false;
    }

    if((user.member_quotation + $scope.freeTime ) < data)
    {
      $scope.isMessages_bootom = true;
      $scope.colorShowMessages = true;
      $scope.message = 'Quotation time for member can’t greater than free time! ';
      return false;
    }

    $http({
          method: 'POST',
          url: ENV.host + '/projects/projects-user-quotation',
          data: {
            user_id: user.id,
            quotation: data,
            project_id: $stateParams.id
          }
        })
        .success(function (data, response) {
          // Close modal when success add user to project.
          $scope.isMessages_bootom = true;
          $scope.colorShowMessages = false;
          $scope.message = data.status;
          $scope.timeOutBootom();
          $scope.getFreeTime();
        })
        .error(function (data, status) {
          // Show error
          $scope.isMessages_bootom = true;
          $scope.colorShowMessages = true;
          $scope.message = data.status;
          $scope.timeOutBootom();
          commonService.timeoutToken(status);  

        });
  }

  $scope.getFreeTime = function ()
  {
    $http({
          method: 'GET',
          url: ENV.host + '/projects/getFreeTime/' + $stateParams.id,
        })
        .success(function (data, response) {
          $scope.freeTime = data.data;
        })
        .error(function (data, status) {
          // console.log(data);
          commonService.timeoutToken(status); 
          $scope.freeTime = 0;
        });
  }
  /**
     * get previous page
     * @return previous page
     */
    // $scope.prevPage = function() {
    // if ($scope.currentPage > 1) {
    //   $scope.currentPage--;
    //   $scope.listUserNotInProject($stateParams.id, $scope.currentPage);
    // }
    // };

    /**
     *  disabled previous page if the index of current page = 1
     */
    // $scope.prevPageDisabled = function() {
    //   return $scope.currentPage === 1 ? "disabled" : '';
    // };

    /**
     * get next page
     * @return next page
     */
    // $scope.nextPage = function() {
    //   if($scope.listUser.length == $scope.userPerPage){
    //     $scope.currentPage++;
    //     $scope.listUserNotInProject($stateParams.id, $scope.currentPage);
    //   }
    // };

    /**
     * disabled next page if the index of current page is last page
     */
    // $scope.nextPageDisabled = function() {
    //   return $scope.listUser.length < $scope.userPerPage ? "disabled" : '';
    // };

});