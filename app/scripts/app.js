'use strict';

/**
 * @ngdoc overview
 * @name TMSApp
 * @description
 * # TMSApp
 *
 * Main module of the application.
 */
angular
  .module('TMSApp')
  .config(function ($stateProvider, $urlRouterProvider, $translateProvider, $validationProvider, ENV) {

    $stateProvider
      .state('app', {
        abstract: true,
        url: '',
        templateUrl: 'views/app.html?v=' + ENV.version,
        controller: 'AppCtrl'
      })
      .state('app.my_dashboard', {
        url: '/my_dashboard',
        views: {
          'appContent': {
            controller: 'MydashboardCtrl',
            templateUrl: 'views/dashboard/mydashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.company_dashboard', {
        url: '/company_dashboard',
        views: {
          'appContent': {
            controller: 'CompanydashboardCtrl',
            templateUrl: 'views/dashboard/companydashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.project_dashboard', {
        url: '/project_dashboard/{project_id}',
        views: {
          'appContent': {
            controller: 'ProjectdashboardCtrl',
            templateUrl: 'views/dashboard/projectdashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.change_password', {
        url: '/change_password',
        views: {
          'appContent': {
            controller: 'UserChangePasswordCtrl',
            templateUrl: 'views/user/change_password.html?v=' + ENV.version
          }
        }
      })
      .state('forgot_password', {
        url: '/forgot_password',
        controller: 'UserForgotPasswordCtrl',
        templateUrl: 'views/user/forgot_password.html?v=' + ENV.version
      })
      .state('login', {
        url: '/login',
        controller: 'UserLoginCtrl',
        templateUrl: 'views/user/login.html?v=' + ENV.version
      })
      .state('app.project_list', {
        url: '/project',
        views: {
          'appContent': {
            controller: 'ProjectListCtrl',
            templateUrl: 'views/project/list.html?v=' + ENV.version
          }
        }
      })
      .state('app.project_disable', {
        url: '/project/{disable}',
        views: {
          'appContent': {
            controller: 'ProjectListCtrl',
            templateUrl: 'views/project/disable_list.html?v=' + ENV.version
          }
        }
      })
      .state('app.report_search', {
        url: '/report/search',
        views: {
          'appContent':{
            controller: 'ReportSearchCtrl',
            templateUrl: 'views/report/search.html?v=' + ENV.version
          }
        }
      })
      .state('app.project_detail', {
        url: '/projects/{id}',
        views: {
          'appContent':{
            controller: 'ProjectDetailCtrl',
            templateUrl: 'views/project/detail.html?v=' + ENV.version
          }
        }
      })
      .state('app.user', {
        url: '/users',
        views: {
          'appContent':{
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/user_dashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.users_register', {
        url: '/users_register',
        views: {
          'appContent':{
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/register_dashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.users_edit', {
        url: '/users_edit/{id}',
        views: {
          'appContent':{
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/edit_dashboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.report_main', {
        url: '/reports/daily/{day}/{month}',
        views: {
          'appContent': {
            controller: 'ReportMainCtrl',
            templateUrl: 'views/report/main.html?v=' + ENV.version
          }
        }
      })
      .state('app.report_missing', {
        url: '/report_missing',
        views: {
          'appContent': {
            controller: 'ReportMissingCtrl',
            templateUrl: 'views/report/missing.html?v=' + ENV.version
          }
        }
      })
      .state('app.report_missing_user', {
        url: '/report_missing/user/{month}',
        views: {
          'appContent': {
            controller: 'ReportMissingReportCtrl',
            templateUrl: 'views/report/missing_report.html?v=' + ENV.version
          }
        }
      })
      .state('app.profile_user', {
        url: '/profile/user/{id}',
        views: {
          'appContent': {
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/profile.html?v=' + ENV.version
          }
        }
      })
      .state('app.setting_dashboard', {
        url: '/setting-dashboard',
        views: {
          'appContent': {
            controller: 'SettingSettingDasboardCtrl',
            templateUrl: 'views/setting/setting_dasboard.html?v=' + ENV.version
          }
        }
      })
      .state('app.User_block', {
        url: '/user-block',
        views: {
          'appContent': {
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/user_block.html?v=' + ENV.version
          }
        }
      })
      .state('app.overtime_management', {
        url: '/overtime_management',
        views: {
          'appContent': {
            controller: 'OvertimeOvertimemanagementCtrl',
            templateUrl: 'views/overtime/overtimemanagement.html?v=' + ENV.version
          }
        }
        })
      .state('app.vacation_user', {
        url: '/vacation_user',
        views: {
          'appContent': {
            controller: 'VacationVacationuserCtrl',
            templateUrl: 'views/vacation/vacationuser.html?v=' + ENV.version
          }
        }
        })
        .state('app.vacation_management', {
        url: '/vacation_management',
        views: {
          'appContent': {
            controller: 'VacationVacationmanagementCtrl',
            templateUrl: 'views/vacation/vacationmanagement.html?v=' + ENV.version  
          }
        }
      })
      .state('app.admin_change_pass', {
        url: '/admin-change-pass/{id}',
        views: {
          'appContent': {
            controller: 'UserDashboardUserCtrl',
            templateUrl: 'views/user/change_pass_user.html?v=' + ENV.version
          }
        }
      })
      .state('app.over-time', {
        url: '/over-time-list',
        views: {
          'appContent': {
            controller: 'OvertimeOvertimeCtrl',
            templateUrl: 'views/overtime/overtime.html?v=' + ENV.version
          }
        }
      });

    $urlRouterProvider.otherwise('/login');

    // Setting for languages.
    $translateProvider.preferredLanguage('en');

    // Initialize for $validationProvider
    $validationProvider.showSuccessMessage = false;

  }).run(function (editableOptions) {

    // Initialize setting for xeditale.
    editableOptions.theme = 'bs3';

  });
