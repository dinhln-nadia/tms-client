'use strict';

/**
 * @ngdoc directive
 * @name TMSApp.directive:confirmAction
 * @description
 * # confirmAction
 */
angular.module('TMSApp')
  .directive('confirmAction', function () {
    return {
      replace: true,
      templateUrl: 'templates/confirmation.html',
      scope: {
        onConfirm: '&'
      },
      controller: function ($scope) {
         $scope.isConfirming = false ;
        $scope.startConfirm = function () {
           $scope.isConfirming = true ;
         };
        $scope.cancel = function () {
          $scope.isConfirming = false ;
        };
        $scope.confirm = function () {
           $scope.onConfirm();
         };
        }
    };
  });
