'use strict';

/**
 * @ngdoc directive
 * @name TMSApp.directive:onFinishRepeat
 * @description
 * # onFinishRepeat
 */
angular.module('TMSApp')
  .directive('onFinishRepeat', function ($rootScope) {
    return function(scope, element, attrs) {
        if (scope.$last){
          scope.onFinishRepeat();
        }
      };
  });
