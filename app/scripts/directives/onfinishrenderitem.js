'use strict';

/**
 * @ngdoc directive
 * @name TMSApp.directive:onFinishRenderItem
 * @description
 * # onFinishRenderItem
 */
angular.module('TMSApp')
  .directive('onFinishRenderItem', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
        if (scope.$last === true) {
          element.ready(function () {
            scope.loadICheck();
          });
        }
      }
    };
  });
