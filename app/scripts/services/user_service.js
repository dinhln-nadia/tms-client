'use strict';

/**
 * @ngdoc service
 * @name TMSApp.UserService
 * @description
 * # UserService
 * Service in the TMSApp.
 */
angular.module('TMSApp')
  .service('UserService', function ($http, ENV, $filter) {

    /**
     * Get data question 
     *
     * @return void
     */
    this.getQuestions = function(next){
      // Send request to QuestionsAPI
      $http({
        method: 'GET',
        url: ENV.host + '/settings/questions',
      })
      .success(function(response){
        // Process data received
        next(response.data);
      })
      .error(function(response){
        // Get data fail
        console.log(response);
        console.log($filter('translate')('services.user.getQuestions.fail'));

        return false;
      });

    };

  });