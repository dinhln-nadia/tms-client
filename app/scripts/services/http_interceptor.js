'use strict';

/**
 * @ngdoc service
 * @name TMSApp.httpResponseInterceptor
 * @description
 * # httpResponseInterceptor
 * Factory in the TMSApp.
 */
angular.module('TMSApp')
  .factory('httpResponseInterceptor', ['$q', '$location', function ($q, $location) {
    return function (promise) {
      var success = function (response) {
        return response;
      };

      var error = function (response) {
        if(response.status === 401 || response.status === 403){
          $localStorage.token = '';
          $location.url('/login');
        }

        return $q.reject(response);
      };

      return promise.then(success, error);
    };
  }]);
angular.module('TMSApp').config(['$httpProvider', function ($httpProvider) {
  $httpProvider.interceptors.push('httpResponseInterceptor');
}]);

/**
 * @ngdoc service
 * @name TMSApp.httpRequestInterceptor
 * @description
 * # httpRequestInterceptor
 * Factory in the TMSApp.
 */
angular.module('TMSApp')
  .factory('httpRequestInterceptor', function ($localStorage) {
    return {
      request: function (config) {
        config.headers = config.headers || {};

        if ($localStorage.token) {
          config.headers.Authorization = 'Bearer ' + $localStorage.token;
        }
        return config;
      }
    };
  });
angular.module('TMSApp').config(function ($httpProvider) {
  $httpProvider.interceptors.push('httpRequestInterceptor');
});