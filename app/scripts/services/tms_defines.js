'use strict';

/**
 * @ngdoc service
 * @name TMSApp.tmsDefines
 * @description
 * # tmsDefines
 * Constant in the TMSApp.
 */
angular.module('TMSApp')
  .constant('tmsDefines', {
    host:'http://localhost/tms-server/public/api/v1'

  });
