'use strict';

/**
 * @ngdoc service
 * @name TMSApp.commonService
 * @description
 * # commonService
 * Service in the TMSApp.
 */
angular.module('TMSApp')
  .service('commonService', function ($http, $localStorage, $location) {

    /**
     * Get data by ping API
     *
     * @return void
     */
    this.requestAPI = function(method, url, data, next){

      // Send request to server
      $http({
        method: method,
        url: url,
        data: data
      })
      .success(function(data, status, headers, config){
        // console.log(data, status, headers, config);
        // Process data received
        next(data.data);
      })
      .error(function(data, status, headers, config){

        if(status === 401 || status === 403 || status === 400){
          $localStorage.token = null;
          $location.url('/login');
        }

      });

    };
    /**
     * check Time format
     * @param  time
     * @param  start or end time
     * @return error or not
     */
    this.checkTime = function (time, message){
      var pattern = new RegExp('^(((([0-1][0-9])|(2[0-3])):?(00|15|30|45))|(24:?00))$');
      var test_time = pattern.test(time);
      if(!test_time){
      return  message + ' must be match with format (blog 15 minutes)';
      }
      return 0;
    };

    /**
     * check Ticket
     * @param  ticket number
     * @return error or not
     */
    this.checkTicket = function (data) {
      if ( /^\+?[0-9][\d]*$/.test(data) !== true) {
        return 'Ticket must be a positive Integer number ';
      }
      if ( /^\+?[0-9][\d]{0,7}$/.test(data) !== true) {
        return 'The ticket may not be greater than 8 characters.';
      }

      return 0;
    };

    /**
     * check null
     * @param  field
     * @param  name field
     * @return error or not
     */
    this.checkNull = function (name, message) {
      if(!name){
       return 'You must insert ' + message ;
      }
      return 0;
    };

    /**
     * check 23:59
     * @param  end time
     * @return true or false
     */
    this.checkNearEndDay = function (end_time) {
        var h = end_time.substr(0,2);
        var i = end_time.substr(3,2);
        if( h =='23' && i == '59'){

          return 1;
        } else {

          return 0;
        }

      };

    /**
     * check 24:00
     * @param end time
     * @return true or false
     */
    this.checkEndDay = function (end_time) {
        var h = end_time.substr(0,2);
        if(h == '24'){

          return 1;
        } else {

          return 0;
        }
      };

    /**
     * Splice row
     * @param  row
     * @param  data
     * @return data
     */
    this.spliceRow = function (report, reports) {
      var index = reports.indexOf(report);
          reports.splice(index, 1);

       return reports;
    };

    /**
     * check float format
     * @param  data
     * @param  name of data
     * @return error or not
     */
    this.checkPositiveFloat = function (data , message) {
      if ( /^(\+)?[0-9]+(\.[0-9]{0,1}[0,5]{0,1})?$/.test(data) !== true) {
      return message + 'Time must be a positive Float number and max 2 letter after the dot (last letter is 0 or 5)';
      }
      return 0;

    };

    /**
     * check max value
     * @return true or false
     */
    this.checkMax = function (data, message) {
      if(parseFloat(data) >= 1000000){
        return message + 'Time must be smaller than 1.000.000 hours';

      }
      return 0;

    };
    /**
     * redirect to login screen when token is experied
     * @param  status
     * @return void
     */
    this.timeoutToken = function (status) {
      if(status === 401 || status === 403 || status === 400){
          $localStorage.token = null;
          $location.url('/login');
        }
    }
      /**
     * change to 1-->01
     * @param  month
     * @return Void
     */
    this.insertStringZero = function (number) {
      if(number < 10){
          number = '0'+ number;
        }
        return number;
    };

  });

