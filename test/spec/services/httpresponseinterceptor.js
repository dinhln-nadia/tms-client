'use strict';

describe('Service: httpResponseInterceptor', function () {

  // load the service's module
  beforeEach(module('TMSApp'));

  // instantiate service
  var httpResponseInterceptor;
  beforeEach(inject(function (_httpResponseInterceptor_) {
    httpResponseInterceptor = _httpResponseInterceptor_;
  }));

  it('should do something', function () {
    expect(!!httpResponseInterceptor).toBe(true);
  });

});
