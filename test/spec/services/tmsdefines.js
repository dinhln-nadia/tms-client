'use strict';

describe('Service: tmsDefines', function () {

  // load the service's module
  beforeEach(module('TMSApp'));

  // instantiate service
  var tmsDefines;
  beforeEach(inject(function (_tmsDefines_) {
    tmsDefines = _tmsDefines_;
  }));

  it('should do something', function () {
    expect(!!tmsDefines).toBe(true);
  });

});
