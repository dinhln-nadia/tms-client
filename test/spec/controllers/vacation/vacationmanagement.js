'use strict';

describe('Controller: VacationVacationmanagementCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var VacationVacationmanagementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VacationVacationmanagementCtrl = $controller('VacationVacationmanagementCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(VacationVacationmanagementCtrl.awesomeThings.length).toBe(3);
  });
});
