'use strict';

describe('Controller: VacationVacationuserCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var VacationVacationuserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VacationVacationuserCtrl = $controller('VacationVacationuserCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(VacationVacationuserCtrl.awesomeThings.length).toBe(3);
  });
});
