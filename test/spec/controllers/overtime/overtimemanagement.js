'use strict';

describe('Controller: OvertimeOvertimemanagementCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var OvertimeOvertimemanagementCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OvertimeOvertimemanagementCtrl = $controller('OvertimeOvertimemanagementCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OvertimeOvertimemanagementCtrl.awesomeThings.length).toBe(3);
  });
});
