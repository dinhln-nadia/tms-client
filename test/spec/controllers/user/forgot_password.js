'use strict';

describe('Controller: UserForgotPasswordCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var UserForgotPasswordCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserForgotPasswordCtrl = $controller('UserForgotPasswordCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
