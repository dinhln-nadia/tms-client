'use strict';

describe('Controller: UserDashboardUserCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var UserDashboardUserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UserDashboardUserCtrl = $controller('UserDashboardUserCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
