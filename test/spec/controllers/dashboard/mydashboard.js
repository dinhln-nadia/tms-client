'use strict';

describe('Controller: DashboardMydashboardCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var DashboardMydashboardCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DashboardMydashboardCtrl = $controller('DashboardMydashboardCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
