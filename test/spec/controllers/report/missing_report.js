'use strict';

describe('Controller: ReportMissingReportCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var ReportMissingReportCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReportMissingReportCtrl = $controller('ReportMissingReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
