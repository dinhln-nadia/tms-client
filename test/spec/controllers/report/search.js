'use strict';

describe('Controller: ReportSearchCtrl', function () {

  // load the controller's module
  beforeEach(module('TMSApp'));

  var ReportSearchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReportSearchCtrl = $controller('ReportSearchCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
