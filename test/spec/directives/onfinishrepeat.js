'use strict';

describe('Directive: onFinishRepeat', function () {

  // load the directive's module
  beforeEach(module('TMSApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<on-finish-repeat></on-finish-repeat>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the onFinishRepeat directive');
  }));
});
