'use strict';

describe('Directive: confirmAction', function () {

  // load the directive's module
  beforeEach(module('TMSApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<confirm-action></confirm-action>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the confirmAction directive');
  }));
});
