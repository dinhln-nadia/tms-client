'use strict';

describe('Directive: onFinishRenderItem', function () {

  // load the directive's module
  beforeEach(module('TMSApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<on-finish-render-item></on-finish-render-item>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the onFinishRenderItem directive');
  }));
});
